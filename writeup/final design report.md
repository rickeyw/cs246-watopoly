# Final Design Document

## Our Design

We generally stuck true to our original design besides a couple differences explained below.

Our implementation of watopoly is based on a few main Classes:

- Controller
- Player
- Piece
- Tile
- TextGrid (View)

We hoped to follow the MVC pattern with the `Tile` and `Piece` classes acting as the Model, `Controller` acting as the Controller, `TextGrid` and 'Player' acting as the view.

### The Piece Class

The `Piece` class represents each of the player's pieces on the board. Each player controls their `Piece` object and each `Piece` stores the player's `money` and owned `Tiles`. 
When a player rolls, their `Piece` object is passed around the board and when they buy/sell property, their `Piece` object gets modified.

### The Tile Class

The `Tile` class represents each `Tile` on the board. It is a virtual class which has two inherited classes, `Building` and `NonBuilding` (Representing ownable/unownable buildings).

We decided to implement this class using the Visitor Pattern by giving every `Tile` object the methods `land` and `visit`. When a `Piece` is moving to a `Tile`, the `Piece` will `visit` each `Tile` on the way, and then `land` on the final tile. It does this by passing itself to the `Tiles` method.

Every `Tile` will have their own functionality for these methods, for example, `Collect OSAP` will give a `Piece` $200 when visited or landed.

#### Buildings

The `Building` class is inherited from the `Tile` class and is also a virtual class, it represents buildings that can be owned. The inherited classes of this class are `AcademicBuilding`, `Gym`, and `Residence`. 

`Building` contains a virtual method `getFee` which will calculate how much each object will charge the `Piece` that lands on it. Based on what type of `Building` it is, the calculation method is different.

##### Improvements

We originally wanted to implement improvements for `AcademicBuildings` using the Observer Pattern. However, after some consulting, we decided it might be a bit overkill. We instead just kept track of the improvement level and respective fees inside the object and incremented whenever it was modified.

##### Building Groups

In order to keep track of which buildings are in groups together, we created a class called `BuildingGroup` for each different group (such as Math, Ev1, Gym, etc) and would keep track of the respective `Buildings` that were part of that group. The `BuildingGroup` was then able to perform any necessary operations such as checking how many a certain `Piece` owned or if a `Building` could be traded.

##### Mortgages

By implementing our `Buildings` using the visitor pattern, it was really simple to add in Mortgaging functionality. All it took was a flag in a `Building` object that would prevent a `Piece` from being charged if it landed on it.

### The Controller Class

To try and stay true to the MVC Pattern, we've implemented the `Controller` class that to keep track of all `Tiles` and `Pieces` in the game. It would also be in charge of any operation that would modify or retrieve anything in the game.

Every time an operation had to be made, such as modifying a `Pieces` cash or changing the ownership of a `Building`, it is done through the `Controller` object. `Controller` will have access to everything and will act as a _buffer_ between different operations.

For example, a trade between **John** and **Bob** with **MC** for **$1000** happens. `Controller` displays a message asking to confirm the trade, receive the input, `Controller` will then proceed to deduct **$1000** from **Bob's** `Piece`, remove **John's** ownership of the **MC** `Building` object and assign **Bob's** `Piece` as the owner.

### The Player Class

The `Player` class is what is in charge of command line arguments and human operations such as different turns, trading, mortgaging, bankruptcy, or improvements. It has methods for each flow that would make calls to the `Controller` to perform the appropriate actions.

Going back to the trade example from the `Controller` class, `Player` would be making the required calls to `Controller` to display the messages, get input and charge money / change ownership. It would also do stuff like checking if a `Piece` has enough money before performing certain actions, or making sure certain cases are met before operations are done.

### The View

To display the board, we implemented the `TextGrid` class to mimic the Observer Pattern. 

It stores every `Tile` as a cell (string) with what it's going to display. Each of these cells contain a visual representation of what `Pieces` currently sit on it, its Improvement level, Mortgage status, Owner, Name. When something in the Model changes, the `Controller` notifies this `TextGrid` object of what `Tile` was affected and how (ex. $ left MC and went to OSAP). The `TextGrid` then updates its local cell to reflect this.

The `TextGrid` then has a method to `draw` the current board and when the `Controller` calls this method, the board is drawn.

## Questions

### Question 1
What lessons did this project teach you about developing software in teams? If you worked
alone, what lessons did you learn about writing large programs?

### Answer 1
This project taught us about the complexities of managing codebase among several individuals.
It's really difficult to keep track of what everybody is working on and try to ensure your work doesn't interfere with another member's. It's also difficult to maintain a _goal_ or overall _picture_ of what everything should look like and attempting to convey that same image to the rest of your group won't come without its challenges.

We've learned that working as a group comes with a lot of compromise. Not everyone has the same idea of what the code should look like and no one person has the _right_ solution. Being able to accept each others' criticisms and comments regarding to how things should be designed will only improve how well a team works.

During the beginning, we all had different coding styles which created some disagreements
amongst us about how things should be done. However, as time progressed we started to formalize our implementations to follow a standard design between us. Leading us to collaborate faster and avoid unnecessary delays.

Another important lesson we learned is how valuable version control really is. During the beginning of our project,
we would just share code with each other and hope everything worked. After our parts kept getting more complicated we decided
to take out work over to GitHub instead. We were able to organize every part of the whole project and create a standard `master` branch to make sure we weren't interfering with each
others' work. We would create pull requests for our individual features and make sure
the rest of the group members approved before they were merged into `master`.

When working on a large project, there are so many little things that can just be completely shortsighted. It's so easy to get obsessed with the idea of _building_ the big project that we lose sight of the small edge cases. Along with this, actually _designing_ before building is something that is very valuable (we learned the hard way). There is a reason design patterns exist and their purpose is to make our lives a lot easier without having to deal with a single file containing 1000 functions.

### Question 2
What would you have done differently if you had the chance to start over?

### Answer 2

A lot of things. First of all, we would have established some policies regarding usage of the software repository. There have been times where teammates pushed changes to the master branch without thorough testing, which caused problems later on. 

This brings us to the second topic -- testing. The need for proper test cases before coding became apparent halfway through our project. We did not take the time to fully digest and, most importantly, reformat the assignment document into a traceable form. 

By poorly organizing the requirements of the project we were left with a lot of small edge cases that were never even paid attention to at the beginning. We ignored the small _if_ cases when designing the project initially as we thought we would just take care of them at the end, not realizing the amount of trouble and headache they would end up causing.

This reduced a lot of the visibility of what people were working on since one has to go through (time and time again) the ten-page long document to find out what the expected behaviour was. Had we kept this in mind at the start of the project we would have designed our implementation around these cases. It would have prevented us from being up at 5 am wondering if SLC did what it's supposed to do.

Another thing is really focusing on reducing coupling. Of course that was our objective at first, but because our original design was so unorganized and shortsighted, we found ourselves deep into a project with only one way of implementing a certain case without having to rewrite our whole design principle. This caused so many problems later on because changing any little thing could've broken everything.

If we had more time, continuous integration methodologies (e.g. Jenkins) would also be used. During the later stages of the project, a new change would've broke an old feature (i.e. function name changes, etc) and we would've been completely oblivious. Having continuous integration would mean we can spot those problems early and often and not make the mistake of pushing a build that broke half our tiles.
