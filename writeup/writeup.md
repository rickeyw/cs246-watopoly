#Plan of Attack

##Questions and Answers

###Question 1

#####Question

After reading this subsection, would the Observer Pattern be a good pattern to use when implementing a gameboard? Why or why not?

#####Answer

The Observer pattern is ideal when it comes to implementing the view of the gameboard. It allows a graphics display to be implemented and modified with ease.

However, for the actual functionalities of the gameboard such as `Tiles` and `Pieces`, the Observer Pattern might not be appropriate. This is because the Observer Pattern is ideal when there is a many-to-one relationship, or when all the tiles "only" need to be notified on a change. Our implementation involves a lot of interaction between the tiles themselves as well as interaction from the tiles to the gameboard.

###Question 2

#####Question

Suppose that we wanted to model SLC and Needles Hall more closely to Chance and Community Chest cards. Is there a suitable design pattern you could use? How would you use it?

#####Answer

The way the SLC and Needles Hall tiles are currently implemented resembles the MVC pattern. If we were to model them closely to Chance and Community Chest, we'd stick with the same implementation. The MVC pattern allows the `Controller` to modify characteristic of the `Model`. Having the SLC and Needles Hall cards tied to the `Controller` allows them to modify the `Model` and its `Pieces` as they feel free.

This means any new rules as defined in the Chance and Community Chest cards (such as moving a player or giving them money) can be done by just appending the rules to the respective tiles.

###Question 3

#####Question

Is the Decorator Pattern a good pattern to use when implementing Improvements? Why or why not?

#####Answer

Yes, by implementing Improvements using the Decorator Pattern we get much more flexiblity within the program. Additions to the buildings are designed dynamically and their attributes (such as how much tuition it adds) can be easily modified. It also makes it easier to keep track of improvements, delete improvements, and create new types of improvements if we were to need to.

##Project Breakdown

###Milestone 1
####Load saved games, Display the board, Allow controller to move and set Piece properties

| Goal | Date (July 2016) | Assignee |
| --- | --- | --- |
|Initial Board | 16th | Ali |
| Graphics View | 16th | Bhargavi |
| Controller Implementation | 17th | Ali |
| Tiles | 17th | Rickey |
| Piece | 17th | Bhargavi |
| Game Initialization | 17th | Ali |

###Milstone 2
####Player Movement, dice rolling, paying tuition

| Goal | Date (July 2016) | Assignee |
| --- | --- | --- |
| Modifying Pieces | 17th | Rickey |
| Tiles using Controller | 18th | Ali |
| Players using Controller | 18th | Bhargavi |

###Milestone 3
####Buying buildings, improving buildings, non-ownable buildings, bankruptcy, auctions

| Goal | Date (July 2016) | Assignee |
| --- | --- | --- |
| NonBuildings as tiles | 20th | Ali |
| Buildings as tiles | 20th | Bhargavi |
| Modify Buildings | 21st | Rickey |
| Additional Building Functionality | 22nd | All |