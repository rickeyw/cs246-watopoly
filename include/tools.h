#include <string>
#include <vector>

class Piece;

std::string itos(const int n);

char stoc(const std::string &s);

bool isint(const std::string &s);

std::vector <std::string> stov(const std::string &s);

void removePieceFromVector(std::vector <Piece *> & vec, Piece * piece);
