#ifndef _PIECE_H_
#define _PIECE_H_

#include <string>
#include <vector>

enum PieceType {
    Goose,
    GRT_Bus,
    TimsDoughnut,
    Prof,
    Student,
    Money,
    Laptop,
    Pinktie,
    BANK,
	PieceType_size
};

class Tile;

class Piece {
    PieceType type = BANK;
    int cash_money;
    int location;
    int jailTime;
    int tims_card = 0;
    std::string name;
    bool can_roll;
    int double_count = 0;

    Tile * landed;
    std::vector <Tile *> owned;
    Piece * owing = nullptr;

public:
    Piece(PieceType type, const std::string &name);

    std::vector <Tile *> getOwned() const;

    void setLanded(Tile * landed);
    Tile * getLanded() const;

    bool isOwner(Tile * building) const;
    void setOwner(Tile * building);
    void removeOwner(Tile *building);

    virtual ~Piece();

    PieceType getType() const;

    int getCashMoney() const;

    void setCashMoney(const int cash);

    int numTimsCard() const;
    void addTimsCard();
    void subTimsCard();

    bool canRoll() const;
    void setRoll(const bool roll);

    void setCash(const int n);

    void setBroke(const bool n);

    std::string getName() const;
    int countAssets() const;

    int getJailTime() const;

    void setJailTime(int jailTime);

    void setLocation(const int location);

    int getLocation() const;

    Tile *getLanded();

    Piece *getOwing() const;
    void setOwing(Piece *p);
    
    void addDouble();
    int getDouble() const;
    void resetDouble();

	static const char PieceTypeToChar(PieceType type);

	static const PieceType CharToPieceType(char input);
};

#endif
