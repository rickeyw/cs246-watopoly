#ifndef _TEXT_GRID_H_
#define _TEXT_GRID_H_
//#include "observer.h"

#include <ostream>
#include <string>
#include <vector>
#include "tile.h"
#include "window.h"

#define TILE_W 9
#define TILE_H 7
#define WORD_WRAP_WIDTH TILE_W - 1
#define BOARD_SZ_X TILE_W*11+2
#define BOARD_SZ_Y TILE_H*11+2

#define  XW_WIDTH 800
#define XW_HEIGHT 800

enum TileType{
	NoInfobox,
	Normal
};

enum TilesBookIndex{
    NAME,
	TILETYPE,
    LEVEL,
    OWNER,
    VISITORS
};

class TextGrid {

    std::vector<std::vector<char>> outbuffer;
    std::vector<std::tuple<std::string, TileType, int, PieceType, std::vector<PieceType>>> tiles_book;

    std::vector<int> getDatum(int location_id);
    std::vector<std::string> wordwrap(std::string input);

	bool construction_mode = false;
	bool xw_available = false;

protected:
    void updateTile(std::string name, TileType location_id, int current_level, int owner_sym, PieceType visitor_sym,
						std::vector<PieceType> vector1);
	std::shared_ptr<Xwindow> xw = nullptr;

public:
    TextGrid();
    virtual ~TextGrid();

	void openXW();

    friend std::ostream &operator<<(std::ostream &out, TextGrid &tg);

    void updateTileById(int id, int new_level);

    void updateTileById(int id, PieceType new_owner_sym);

    void updateTileById(PieceType new_visitor, int old_location_id, int new_location_id);

	void removePlayerFromTile(PieceType new_visitor, int location_id);

	void constructTile(std::string name, int location_id, int current_level, PieceType owner_sym,
					   std::vector<PieceType> visitor_sym);
	void drawWindow();

	void enterConstructionMode();

	bool isConstruction_mode() const;

	void exitConstructionMode();

	void BookUpdate();


};

#endif
