#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_
#include "tile.h"
#include "piece.h"
#include "text_grid.h"
#include <string>
#include <memory>
#include <vector>


class BuildingGroup;
class Player;

class Controller {
	std::vector <Piece *> pieces;
	std::vector <Tile *> tiles;
	bool test_mode;
	int disp_mode; // Display mode. 1 For text only, 2 For text and Xwindow, 3 For Xwindow only
	std::string save_file = "";
	std::vector <BuildingGroup *> groups;
	std::shared_ptr<TextGrid> tg;
	unsigned long random_seed;

	Player * player;

	int tims_cards;

	int current_turn;

	void initPieces();
	void initTiles();

	void improveBuildingOverride(Tile &building);

public:
	void init();
	Controller(bool testMode, int disp_mode, std::string save_file, unsigned long random_seed);
	~Controller();

	unsigned long getRandomSeed() const;

	std::vector <Piece *> getPieces() const;

	void nextTurn();
	Piece * getCurrentPiece() const;
	BuildingGroup * getGroupByName(const std::string & name);
	int getNumPieces() const;
	void setCash(Piece &piece, const int cash);
	void addCash(Piece &piece, const int cash);
	void subCash(Piece &piece, const int cash, Piece * to = nullptr);

	int getCash(Piece &piece);

	int askQuestion(const std::string &question, int min, int max, bool yesno = false);

	void displayMsg(std::string msg);
	std::string getInput();

	int countAssets(Piece &piece);

	void setJail(Piece &piece, const int in_jail);

	void movePiece(Piece &piece, Tile &tile);
	void movePiece(Piece &piece, const int n);
	void movePieceByID(Piece &piece, const int tile_id);

	void walkPiece(Piece &piece, const int n);
	void walkPiece(Piece &piece, Tile &tile);

	int numTimsCup(Piece &piece);
	bool giveTimsCup(Piece &piece);
	bool useTimsCup(Piece &piece);

	void subTimsCup(int n);

	void setBuildingOwner(Piece &piece, Tile &building);

	// needs to be a tile, not a building because we'd be upcasting after looping
	Tile * getBuildingByName(const std::string &name);
	Piece * getPieceByName(const std::string &name);

	bool canPieceRoll(Piece &p) const;
	void setPieceRoll(Piece &p, const bool roll);
	void improveBuilding(Tile &building);
	void unimproveBuilding(Tile &building);

	bool owns(Piece &p, Tile *building);
	void save(const std::string filename);
	void auction(Tile &building);

	bool canImproveBuilding(Tile &building);
	int getImproveCost(Tile &building);
	int getImproveLevel(Tile &building);

	bool initPiecesCore(std::string &PlayerName, char PlayerType);
	void loadSavedGame(std::string &file_name);

	std::vector <Tile *> getBuildings(Piece &p);
	std::vector <Piece *> getPieces();
	int dice();

	bool isMortgaged(Tile & building);
	void mortgage(Tile & building);
	int getCost(Tile & building);
	void unmortgage(Tile &building);

	bool isBroke(Piece &p);
	Piece * getTileOwner(Tile &tile) const;
	void removeFromGame(Piece &p);
	Piece * getOwing(Piece &p) const;

	std::string getName(Tile &tile) const;
	std::string getName(Piece &piece) const;

	void test_tg_cout();

	int getTileIDByReference(Tile &tile);

	bool isTest() const;

	int getJail(Piece &piece);
	int getNumBuildings() const;

	void tgDraw();
	bool canTradeBuilding(Tile &building) const;
};

#endif
