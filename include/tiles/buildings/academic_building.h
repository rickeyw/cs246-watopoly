#ifndef _A_BUILDING_H_
#define _A_BUILDING_H_
#include "building.h"
#include <string>
#include <vector>

class AcademicBuilding : public Building {
	int improve_cost;
	int level;

public:
	AcademicBuilding(Controller * ctrl, const std::string &name, BuildingGroup * group, 
		const int cost, std::vector <int> levels, const int improve_cost);
	~AcademicBuilding();
	
	bool isMonopoly() const;

	int getLevel() const override;
	void addLevel() override;
	void subLevel() override;
	int getImproveCost() const override;
	bool canImprove() override;

	int getFee() override;
	int getNetValue() const override;
	bool canTrade() const override;
};

#endif
