#ifndef _BUILDING_GROUP_H_
#define _BUILDING_GROUP_H_
#include "piece.h"

#include <string>
#include <vector>

class Building;

class BuildingGroup {
	std::string name;

	std::vector <Building*> buildings;

public:
	BuildingGroup(const std::string &name);

	~BuildingGroup();

	int getBuildingsByOwner(Piece &piece);

	std::string getName() const;
	void addBuilding(Building *);
	int getSize() const;
	bool checkNoImproves() const;
};

#endif
