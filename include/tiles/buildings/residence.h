#ifndef _RESIDENCE_H_
#define _RESIDENCE_H_
#include "building.h"
#include <string>
#include <vector>

class Residence : public Building {
public:
	Residence(Controller * ctrl, const std::string &name, BuildingGroup * group, 
		const int cost, std::vector <int> levels);
	~Residence();
	int getFee() override;
};

#endif
