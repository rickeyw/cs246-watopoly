#ifndef _GYM_H_
#define _GYM_H_
#include "building.h"
#include <string>
#include <vector>

class Gym : public Building {
public:
	Gym(Controller * ctrl, const std::string &name, BuildingGroup * group,
		const int cost, std::vector <int> levels);
	~Gym();
	int getFee() override;
};

#endif
