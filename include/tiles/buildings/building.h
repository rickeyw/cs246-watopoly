#ifndef _BUILDING_H_
#define _BUILDING_H_
#include "tile.h"

#include <string>
#include <vector>

class BuildingGroup;

class Building : public Tile {
protected:
	BuildingGroup * group;
	int cost;
	std::vector <int> levels;
	Piece* owner;

	bool sold, mortgaged;
public:
	Building(Controller * ctrl, const std::string &name, BuildingGroup * group, 
		const int cost, std::vector <int> levels);
	~Building();

	void mortgage() override;
	void unmortgage() override;
	bool isMortgaged() const override;

	Piece * getOwner() const override;
	void setOwner(Piece*) override;

	bool isSold() const;
	void setSold(bool value);

	int getCost() const override;

	virtual int getFee() = 0;
	virtual int getNetValue() const override;

	void land(Piece &p) override;
	void visit(Piece &p) override;

	void setGroup(BuildingGroup * g);
};

#endif
