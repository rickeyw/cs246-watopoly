#ifndef _OSAP_H_
#define _OSAP_H_

#include "nonbuilding.h"

class Osap: public NonBuilding {
	const int money;

public:
	Osap(Controller * ctrl, const std::string &name, const int money = 200);
	~Osap();
	
	void land(Piece &p) override;
	void visit(Piece &p) override;
};

#endif
