#ifndef _PAY_TUITION_H_
#define _PAY_TUITION_H_

#include "nonbuilding.h"
#include "piece.h"

class PayTuition: public NonBuilding {
    const int cost;

public:
    PayTuition(Controller * ctrl, const std::string &name, const int cost = 300);
    ~PayTuition();

    void land(Piece &piece) override;

};

#endif
