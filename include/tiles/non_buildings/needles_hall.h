#ifndef _NEEDLES_HALL_H_
#define _NEEDLES_HALL_H_

#include "nonbuilding.h"

class NeedlesHall: public NonBuilding {
	int payment_calc();
	bool debug;
public:
    NeedlesHall(Controller * ctrl, const std::string &name, bool debug=false);
    ~NeedlesHall();

    void land(Piece &piece) override;
};


#endif
