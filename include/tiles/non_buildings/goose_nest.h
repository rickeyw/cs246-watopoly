#ifndef _GOOSE_NEST_H_
#define _GOOSE_NEST_H_

#include "nonbuilding.h"

class GooseNest: public NonBuilding {
    
public:
    GooseNest(Controller * ctrl, const std::string &name);
    ~GooseNest();

    void land(Piece &p) override;
};


#endif
