#ifndef _TIMS_LINE_H_
#define _TIMS_LINE_H_

#include "nonbuilding.h"

class TimsLine: public NonBuilding {
	const int coffee_price;

public:
    TimsLine(Controller * ctrl, const std::string &name, const int cost = 50);
    ~TimsLine();

    void land(Piece &pc) override;

	void process_repeat_inmate(Piece &pc, int sum_last_roll);
};

#endif
