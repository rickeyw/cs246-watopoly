#ifndef _COOP_FEE_H_
#define _COOP_FEE_H_

#include "nonbuilding.h"

class CoopFee: public NonBuilding {
	const int cost;

public:
	CoopFee(Controller * ctrl, const std::string &name, const int cost=150);
	~CoopFee();

	void land(Piece &p) override;
};

#endif
