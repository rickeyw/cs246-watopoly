#ifndef _NON_BUILDING_H_
#define _NON_BUILDING_H_

#include "tile.h"

class NonBuilding: public Tile {
public:
	NonBuilding(Controller * ctrl, const std::string &name);
	~NonBuilding();

};

#endif
