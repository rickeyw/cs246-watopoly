#ifndef _GOTO_TIMS_H_
#define _GOTO_TIMS_H_

#include "nonbuilding.h"

class GotoTims: public NonBuilding {

public:
	GotoTims(Controller * ctrl, const std::string &name);
	~GotoTims();

    void land(Piece &p) override;
    void visit(Piece &p) override;
};

#endif
