#ifndef _SLC_H_
#define _SLC_H_

#include "nonbuilding.h"

class Slc : public NonBuilding {
	bool debug;
public:
    Slc(Controller * ctrl, const std::string &name, bool debug=false);
    ~Slc();

    void land(Piece &p) override;
};

#endif
