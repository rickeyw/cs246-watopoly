#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "piece.h"
#include <vector>
#include <random>

class Controller;

class Player {
	Controller * ctrl;
	std::default_random_engine random_engine;

	int dice();

	void roll(Piece &p, int d1 = -1, int d2 = -1);
	bool next(Piece &p);

	void trade(Piece &p, const std::string &name, const std::string &b1, const std::string &b2);
	void help();
	void improve(Piece &p, const std::string &name, const std::string &buysell);

	void mortgage(Piece &p, const std::string &name);
	void unmortgage(Piece &p, const std::string &name);

	bool bankrupt(Piece &p);

	void assets(Piece &p);
	void all(Piece &p);

	void save(const std::string &filename);

	void nextTurn();
	void testing(Piece &p, std::vector <std::string> commands);
public:
	Player(Controller * ctrl);

	void startTurn(Piece &p);
    void auction(Tile &building);
    int die();
};

#endif
