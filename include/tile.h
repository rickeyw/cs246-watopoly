#ifndef _TILES_H_
#define _TILES_H_

#include "piece.h"

#include <string>
#include <vector>
#include <memory>
#include <random>

class Controller;

class Tile {
protected:
	Controller * ctrl;
	const std::string name;
	int cost;
	std::default_random_engine randomEngine;

	std::vector <Piece *> pieces;

public:
	Tile(Controller * ctrl, const std::string &name);

	virtual ~Tile() = 0;

	virtual void land(Piece &p) = 0;
	virtual void visit(Piece &p);

	virtual void mortgage();
	virtual void unmortgage();
	virtual bool isMortgaged() const;

	virtual int getCost() const;

	virtual void setOwner(Piece *p);
	virtual Piece * getOwner() const;

	virtual int getLevel() const;
	virtual void addLevel();
	virtual void subLevel();
	virtual int getImproveCost() const;
	virtual bool canImprove();
	virtual int getNetValue() const;
	virtual bool canTrade() const;

	std::string getName() const;

	std::vector<Piece *> getPieces() const;
};
#endif
