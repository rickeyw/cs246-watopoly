CXX=g++-5
CXXFLAGS=-std=c++14 -Wall -MMD -g -Werror=vla -lX11 -Iinclude -Iinclude/tiles -Iinclude/tiles/buildings -Iinclude/tiles/non_buildings
EXEC=watopoly

BUILDINGS=src/tiles/buildings/academic_building.o src/tiles/buildings/building.o src/tiles/buildings/building_group.o src/tiles/buildings/gym.o src/tiles/buildings/residence.o

NONBUILDINGS=src/tiles/non_buildings/coop_fee.o src/tiles/non_buildings/goose_nest.o src/tiles/non_buildings/goto_tims.o src/tiles/non_buildings/needles_hall.o src/tiles/non_buildings/osap.o src/tiles/non_buildings/pay_tuition.o src/tiles/non_buildings/slc.o src/tiles/non_buildings/tims_line.o src/tiles/non_buildings/nonbuilding.o

MAIN=src/controller.o src/piece.o src/player.o src/tools.o src/tile.o src/main.o  src/text_grid.o src/window.o

OBJECTS=${MAIN} ${BUILDINGS} ${NONBUILDINGS}
DEPENDS=${OBJECTS:.o=.d}

${EXEC}: ${OBJECTS}
	${CXX} ${OBJECTS} -o ${EXEC} ${CXXFLAGS}
	
-include ${DEPENDS}

clean:
	rm ${OBJECTS} ${DEPENDS} ${EXEC}
