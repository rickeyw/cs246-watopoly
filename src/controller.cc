#include "controller.h"
#include <sstream>
#include <fstream>
#include "tools.h"
#include <iostream>
#include <algorithm>

#include "academic_building.h"
#include "gym.h"
#include "residence.h"

#include "building_group.h"

#include "player.h"

#include "coop_fee.h"
#include "goose_nest.h"
#include "goto_tims.h"
#include "needles_hall.h"
#include "osap.h"
#include "pay_tuition.h"
#include "slc.h"
#include "tims_line.h"
using namespace std;

Controller::Controller(bool testMode, int disp_mode, string save_file, unsigned long seed) :
		test_mode{testMode}, disp_mode{disp_mode}, save_file{save_file}, random_seed{seed} { }

void Controller::initTiles() {
    ifstream gamedata {"data/buildings.dat"};
    if(!gamedata.good()) throw runtime_error("buildings.dat not accessible");

    string line;
    while (getline(gamedata, line)) {
        istringstream building_data {line};

        string data;
        int i = 0;

        string name;
        string group;
        int cost = 0;
        int icost = 0;
        vector <int> fees;

        while (getline(building_data, data, ',')) {
            if (i == 0) name = data;
            if (i == 1) group = data;
            if (group == "NONE") break;

            if (i == 2) cost = stoi(data);

            if ((i == 3 && (group == "RES" || group == "GYM")) || i == 4) {
                istringstream fees_ss{ data };
                int level;
                while (fees_ss >> level) {
                    fees.push_back(level);
                }
                break;
            }
            else if (i == 3) {
                icost = stoi(data);
            }
            ++i;
        }
        
        if (group == "NONE") {
            NonBuilding * nonbuilding = nullptr;

            if (name == "COLLECT OSAP") {
                nonbuilding = new Osap{this, name};
            }
            if (name == "DC TIMS LINE") {
                nonbuilding = new TimsLine{this, name};
            }
            if (name == "GO TO TIMS") {
                nonbuilding = new GotoTims{this, name};
            }
            if (name == "GOOSE NESTING") {
                nonbuilding = new GooseNest{this, name};
            }
            if (name == "TUITION") {
                nonbuilding = new PayTuition{this, name};
            }
            if (name == "COOP FEE") {
                nonbuilding = new CoopFee{this, name};
            }
            if (name == "SLC") {
                nonbuilding = new Slc{this, name, test_mode};
            }
            if (name == "NEEDLES HALL") {
                nonbuilding = new NeedlesHall{this, name, test_mode};
            }

            tiles.push_back(nonbuilding);

        } else {
            Building * building = nullptr;

            if (group == "GYM") {
                BuildingGroup* g = getGroupByName(group);
                building = new Gym{ this, name, g, cost, fees };
                g->addBuilding(building);
            }
            else if (group == "RES") {
                BuildingGroup* g = getGroupByName(group);
                building = new Residence{ this, name, g, cost , fees };
                g->addBuilding(building);
            }
            else {
                BuildingGroup* g = getGroupByName(group);
                building = new AcademicBuilding{ this, name, g, cost, fees, icost};
                g->addBuilding(building);
            }

            tiles.push_back(building);
        }
		building_data.clear();
    }
	gamedata.close();
}

void Controller::init() {
	// Prepare text grid first (loadSavedGame will call tg)
	tg = std::make_shared<TextGrid>();
    tg.get()->enterConstructionMode();
	if(disp_mode > 1) tg.get()->openXW();

	// Data structures init
	this->initTiles();
	if(save_file.size() > 0){
		this->loadSavedGame(save_file);
	} else {
		this->initPieces();
		for (auto &piece : pieces) {
			piece->setLocation(0);
			setCash(*piece, 1500);
		}
	}

	// Display init
	int location = 0;
	for (auto t : tiles){
		PieceType owner_pieceType;
		if (t->getOwner() == nullptr){
			owner_pieceType = BANK;
		} else {
			owner_pieceType = t->getOwner()->getType();
		}
		tg.get()->constructTile(t->getName(), location, t->getLevel(), owner_pieceType, {});
		tg.get()->updateTileById(getTileIDByReference(*t), t->isMortgaged() ? -1 : t->getLevel());
		++location;
	}
    for (auto &piece : pieces) {
		if(piece->getType() != BANK) tg.get()->updateTileById(piece->getType(), 0, piece->getLocation());
    }
    tg.get()->exitConstructionMode();

    player = new Player{this};
    current_turn = 0;
    player->startTurn(*pieces[0]);
}

void Controller::nextTurn() {
    ++current_turn;
    current_turn = current_turn % pieces.size();
    Piece * current_piece = pieces[current_turn];

    player->startTurn(*current_piece);
}

Piece * Controller::getCurrentPiece() const {
    return pieces[current_turn];
}

void Controller::setCash(Piece &piece, const int cash) {
    piece.setCashMoney(cash);
}

Controller::~Controller() {
    for (auto &piece : pieces) {
        delete piece;
    }
    for (auto &tile : tiles) {
        delete tile;
    }
    for (auto &group : groups) {
        delete group;
    }
    delete player;
}


void Controller::loadSavedGame(string &file_name) {
    vector<string> token;

    ifstream infile(file_name);
    if(!infile.good()) throw runtime_error("The saved file is not accessible");
    string line;
    getline(infile, line);

    // Players
    int numPlayers = stoi(line);
    for(int i = 0; i<numPlayers; ++i){
        getline(infile, line);
        istringstream iss(line);
        string s;
        while (iss >> s) token.push_back(s); // Tokenize
        try {
            if(test_mode){
                cout << "name = " << token.at(0);
                cout << " char = " << token.at(1);
                cout << " TimsCups = " << token.at(2);
                cout << " money = " << token.at(3);
                cout << " position = " << token.at(4) << endl;
            }

            initPiecesCore(token.at(0), stoc(token.at(1)));
            Piece *pc = getPieceByName(token.at(0));

            for(int k = 0; k<stoi(token.at(2)); k++){
                giveTimsCup(*pc);
            }

            setCash(*pc, stoi(token.at(3)));

            /*
             * A player can not start on square 30 (Go to DC Tims Line).
             * If a player is on square 10 (DC Tims Line), their line will look like one of the following:
             * player char TimsCups money 10 0
             * player char TimsCups money 10 1 num
             * The first line represents that the player’s position is the DC Tims Line but they are not actually in the
             * DC Tims Line. The second line represents the player is in the DC Tims line and num is the number of turns
             * they’ve been there. The value of num must be between 0 and 2, inclusive.
             */
            if(stoi(token.at(4)) == 30){
                cout << "Player " << token.at(0) << " cannot start at Go To DC Tims Line" << endl;
                abort();
            } else if (stoi(token.at(4)) == 10){
                if(stoi(token.at(5)) == 1){
                    pc->setJailTime(stoi(token.at(6)) + 1); // Conversion from save file to tims_line implementation
                }
            }
            pc->setLocation(stoi(token.at(4)));
            token.clear();
        } catch (out_of_range &ex){
            cout << "Save file players invalid. Out of range. " << endl;
            abort();
        }
		iss.clear();
    }


    // Building
    while(getline(infile, line)){
        istringstream iss(line);
        string s;
        while (iss >> s) token.push_back(s);
        try {
            if (test_mode) {
                cout << "bldg name = " << token.at(0);
                cout << " own = " << token.at(1);
                cout << " level = " << token.at(2) << endl;
            }
            Piece *pc = getPieceByName(token.at(1));
            Tile *bldg = getBuildingByName(token.at(0));

            if(pc) setBuildingOwner(*pc, *bldg); //pc is Null when it is BANK.
            for (int i = 0; i < stoi(token.at(2)); ++i){
                improveBuildingOverride(*bldg);
            }
			if(stoi(token.at(2)) == -1) mortgage(*bldg);
            token.clear();
        } catch (out_of_range &ex){
            cout << "Save file buildings invalid. Out of range. " << endl;
            abort();
        }
		iss.clear();
    }

	infile.close();
}


/**
 * Interactively get player info
 */
void Controller::initPieces() {
    ostringstream message;
    message << "How many players?" << endl;
    message << "Choose between 2 and 8";

    int response = askQuestion(message.str(), 2, 8);

    string display_pieces = "G B D P S $ L T";


    for (int i {0}; i < response; ++i) {
        displayMsg("Enter name for player " + to_string(i+1));
        string name = getInput();

        displayMsg("Choose a Piece");
        displayMsg(display_pieces);

        char p_response = stoc(getInput());

        if(!initPiecesCore(name, p_response)){
            displayMsg("Please try again: ");
            --i;
        }
    }
}

/**
 * Initializes a piece according to the parameter. Each call creates ONE new piece only.
 * Can be called as many times as needed to create new player.
 * @param PlayerName
 * @param PlayerType
 */
bool Controller::initPiecesCore(string &PlayerName, char PlayerType) {
    // Check for duplicate -- PieceType
    for(auto p : pieces){
        if(p->getName() == PlayerName || p->getType() == Piece::CharToPieceType(PlayerType)) {
			displayMsg("You can't choose same name or piece as someone else. ");
			return false;
		}
    }

	if(Piece::CharToPieceType(PlayerType) >= BANK) {
		displayMsg("Your piece choice was not valid.");
		return false;
	}

    if(PlayerName.empty() || PlayerName.find_first_not_of (' ') == PlayerName.npos){
        displayMsg("Your name is too short. ");
        return false;
    }
    
    	auto caseInsCharCompareN = [&](char a, char b) { return(toupper(a) == toupper(b)); };
	auto caseInsCompare= [&](const string& s1, const string& s2) {
				 return((s1.size() == s2.size()) &&
						  equal(s1.begin(), s1.end(), s2.begin(), caseInsCharCompareN));
		};
	if(caseInsCompare(PlayerName, "bank")) {
		displayMsg("You cannot be the Bank. ");
		return false;
	}

    Piece * piece = new Piece {PieceType(Piece::CharToPieceType(PlayerType)), PlayerName};
    pieces.push_back(piece);
    return true;
}

void Controller::addCash(Piece &piece, const int cash) {
    int curr_money = piece.getCashMoney();
    int new_money = curr_money + cash;

    piece.setCashMoney(new_money);

    displayMsg(getName(piece) + " gained $ " + to_string(cash));

    if (curr_money < 0) {
        Piece * owing = getOwing(piece);
        int pay_back = cash;

        if (new_money >= 0) pay_back = -curr_money;

        if (owing == nullptr) {
            displayMsg(getName(piece) + " pays back the bank $" + to_string(pay_back));
        } else {
            displayMsg(getName(piece) + " pays back " + getName(*owing) + " $" + to_string(pay_back));
            addCash(*owing, pay_back);
        }
    }

}

BuildingGroup * Controller::getGroupByName(const string &name) {
	for (auto g : groups) {
		if (g->getName() == name) return g;
	}
    BuildingGroup * g = new BuildingGroup{name};
    groups.push_back(g);
    return g;
}

void Controller::subCash(Piece &piece, const int cash, Piece * to) {
    int curr_money = piece.getCashMoney();
    int new_money = curr_money - cash;

    piece.setCashMoney(new_money);
    displayMsg(getName(piece) + " lost $ " + itos(cash));

    if (new_money < 0) {
        piece.setOwing(to);

        if (to == nullptr) {
            displayMsg(getName(piece) + " owes the bank $" + to_string(-new_money));
        } else {
            displayMsg(getName(piece) + " owes " + getName(*to) + " $" + to_string(-new_money));
        }

        displayMsg(getName(piece) + " has to declare bankruptcy or raise enough money");
    }
}

int Controller::getCash(Piece &piece) {
    return piece.getCashMoney();
}

int Controller::countAssets(Piece &p) {
    return p.countAssets();
}

void Controller::setJail(Piece &piece, const int in_jail) {
    piece.setJailTime(in_jail);
}

int Controller::getJail(Piece &piece){
    return piece.getJailTime();
}

void Controller::movePiece(Piece &piece, const int n) {
    int loc = piece.getLocation() + n;

    movePiece(piece, *tiles[loc % 40]);
}

void Controller::movePieceByID(Piece &piece, const int tile_id) {
    tg.get()->updateTileById(piece.getType(), piece.getLocation(), tile_id);
    piece.setLocation(tile_id);

	movePiece(piece, *tiles[tile_id % 40]);
}

void Controller::movePiece(Piece &piece, Tile &tile) {
    displayMsg(getName(piece) + " lands on " + getName(tile));

    int id = getTileIDByReference(tile);

    tg.get()->updateTileById(piece.getType(), piece.getLocation(), id);
    piece.setLocation(id);
    tg.get()->drawWindow();

    tile.land(piece);
}

/**
 * Test cases: n = 0, n = 1, n = -1, n = positive number, n = negative number
 * And start at tile number 1 and move back a few tiles.
 * @param piece The pice to walk
 * @param n Number
 */
void Controller::walkPiece(Piece &piece, const int n) {
	const int current_location = piece.getLocation();
	int step = -1;
	if (n > 0) step = 1;

	auto better_mod = [](int a, int b) { return (a % b + b) % b; };

	int next_tile = better_mod((current_location + step), 40);
	int destination = better_mod((current_location + n), 40);

	for (int i{next_tile}; i != destination; i = better_mod((i + step), 40)) {
		tg.get()->updateTileById(piece.getType(), piece.getLocation(), i);
		piece.setLocation(i);
		tiles[i % 40]->visit(piece);
	}

	tg.get()->updateTileById(piece.getType(), piece.getLocation(), destination);
	piece.setLocation(destination);

	movePiece(piece, *tiles[destination]);
}

//@todo implement me!
void Controller::walkPiece(Piece &piece, Tile &tile) {

}

int Controller::askQuestion(const string &question, int min, int max, bool yesno) {
    displayMsg(question);

    istringstream ss;
    ss.exceptions(ios::failbit);

    if (yesno) {
        while (1) {
            string response = getInput();
            if (response == "y") return 1;
            if (response == "n") return 0;
            displayMsg("Please enter valid input");
        }
    } else {
        while (1) {
            string response = getInput();

            if (isint(response)) {
                int i = stoi(response);
                if (i >= min && i <= max) return i;
            }
            
            displayMsg("Please enter valid input");
        }
    }
}

int Controller::numTimsCup(Piece &piece) {
    return piece.numTimsCard();
}

bool Controller::giveTimsCup(Piece &piece) {
    if (tims_cards == 4) {
		displayMsg(getName(piece) + " finds a Roll up the Rim cup but it's filled with corrosive tears of CS 246 students so it is useless. ");
		return false;
    }
    displayMsg(getName(piece) + " has gained a Roll up the Rim cup");
    piece.addTimsCard();

    ++tims_cards;

    return true;
}

bool Controller::useTimsCup(Piece &piece) {
    if (piece.numTimsCard() == 0) {
        displayMsg("You do not have a cup!");
        return false;
    }

    piece.subTimsCard();
    --tims_cards;
    return true;
}

void Controller::subTimsCup(int n) {
    tims_cards -= n;
}

void Controller::setBuildingOwner(Piece &piece, Tile &building) {
    displayMsg(getName(piece) + " now owns " + getName(building));
    building.setOwner(&piece);

    if (isMortgaged(building)) {
        displayMsg(getName(piece) + " has gained a mortgaged building, they must pay 10% of its value");
        subCash(piece, getCost(building) * 10 / 100);

        int response = askQuestion("Do you wish to unmortgage this building for 50% of its value? (y/n)", 0, 1, true);

        if (response) {
            if (getCash(piece) < (getCost(building) * 5/10)) {
                displayMsg("You dont have enough money!");
            } else {
                subCash(piece, getCost(building) * 5/10);
                unmortgage(building);
                displayMsg(getName(piece) + " has unmortgaged " + getName(building));
            }
        }
    }

	tg.get()->updateTileById(getTileIDByReference(building), piece.getType());
	tg.get()->drawWindow();
}

Tile * Controller::getBuildingByName(const string &name) {
    for (auto tile : tiles) {
        if (getName(*tile) == name) {
            return tile;
        }
    }
    return nullptr;
}

int Controller::getTileIDByReference(Tile &tile){
	Tile *tmp = &tile;
	for(size_t i {0}; i < tiles.size(); ++i){
		if(tiles.at(i) == tmp){
			return i;
		}
	};

	throw runtime_error("In getTileIDByReference, the Tile is not valid");
}

Piece * Controller::getPieceByName(const string &name) {
    for (auto piece : pieces) {
        if (getName(*piece) == name) {
            return piece;
        }
    }
    return nullptr;
}

bool Controller::canPieceRoll(Piece &p) const {
    return p.canRoll();
}

void Controller::setPieceRoll(Piece &p, const bool roll) {
    p.setRoll(roll);
}

void Controller::displayMsg(string msg) {
    cout << msg << endl;
}

string Controller::getInput() {
    cin.exceptions(ios::failbit | ios::eofbit);

    string line;

    try {
        getline(cin, line);
    } catch (ios::failure &) {
        displayMsg("Watopoly is shutting down");
        exit(0);
    }
    return line;
}

int Controller::dice() {
    return player->die();
}

void Controller::auction(Tile &building) {
    player->auction(building);
}

bool Controller::canImproveBuilding(Tile &building) {
    return building.canImprove();
}

int Controller::getImproveCost(Tile &building) {
    return building.getImproveCost();
}

int Controller::getImproveLevel(Tile &building) {
    return building.getLevel();
}

void Controller::improveBuilding(Tile &building) {
    if (canImproveBuilding(building)) {
        building.addLevel();
		tg.get()->updateTileById(getTileIDByReference(building), building.getLevel());
    }
}

/**
 * For use by the initializer only
 * @param building
 */
void Controller::improveBuildingOverride(Tile &building) {
		building.addLevel();
		tg.get()->updateTileById(getTileIDByReference(building), building.getLevel());

}

void Controller::unimproveBuilding(Tile &building) {
    if (getImproveLevel(building) > 0) {
        building.subLevel();
		tg.get()->updateTileById(getTileIDByReference(building), building.getLevel());
    }
}

vector <Piece *> Controller::getPieces() const {
    return pieces;
}

bool Controller::owns(Piece &p, Tile * building) {
    return p.isOwner(building);
}

vector <Tile *> Controller::getBuildings(Piece &p) {
    return p.getOwned();
}

bool Controller::isMortgaged(Tile &building) {
    return building.isMortgaged();
}

void Controller::mortgage(Tile &building) {
    building.mortgage();
	tg.get()->updateTileById(getTileIDByReference(building), building.isMortgaged() ? -1 : building.getLevel());
}

int Controller::getCost(Tile &building) {
    return building.getCost();
}

void Controller::unmortgage(Tile &building) {
    building.unmortgage();
	tg.get()->updateTileById(getTileIDByReference(building), building.isMortgaged() ? -1 : building.getLevel());
}

vector <Piece *> Controller::getPieces() {
    return pieces;
}

bool Controller::isBroke(Piece &p) {
    return getCash(p) < 0;
}

Piece * Controller::getTileOwner(Tile &tile) const {
    return tile.getOwner();
}

void Controller::removeFromGame(Piece &p) {
    removePieceFromVector(pieces, &p);

    displayMsg(getName(p) + " is out of the game!");
	tg.get()->removePlayerFromTile(p.getType(), p.getLocation());
    delete &p;

    if (pieces.size() == 1) {
        displayMsg(getName(*pieces[0]) + " wins the game!");
        exit(0);
    }
}

Piece * Controller::getOwing(Piece &piece) const {
    return piece.getOwing();
}

string Controller::getName(Tile &tile) const {
    return tile.getName();
}

string Controller::getName(Piece &piece) const {
    return piece.getName();
}

bool Controller::isTest() const {
    return test_mode;
}


/**
 * TextGrid output testing function
 */
void Controller::test_tg_cout() {
	cout << *tg;
}

unsigned long Controller::getRandomSeed() const {
	return random_seed;
}

int Controller::getNumBuildings() const {
        int n = 0;
  	for (auto p : tiles) {
   		if(p->getCost() != 0) { 
    			++n;
     		}
      	}
     	return n;
}

void Controller::save(const std::string file){
     fstream s;
     s.open(file, fstream::out);
     s<< pieces.size() << endl;
     for(size_t i  = 0; i < pieces.size(); ++i){
        Piece * p = pieces[i];
        s << p->getName()<< " " << Piece::PieceTypeToChar(p->getType())  << " " << p->numTimsCard() << " "<<
        p->getCashMoney() << " "<< p->getLocation();

        if( p->getLocation() == 10){

            if (p->getJailTime() == 0) {
                s << " " <<  0;
            } else {
                s<<" "<<1<<" "<<p->getJailTime() - 1;
            }
        }
     s << endl;
    }

     
    for(int i = 0; i < getNumBuildings(); ++i){
	   if(tiles[i]->getCost() != 0) {
            s << tiles[i]->getName() << " ";

            if (tiles[i]->getOwner() != nullptr) {
                s << tiles[i]->getOwner()->getName() << " ";
            } else {
                s<<"BANK" << " ";
	     	}

            if (tiles[i]->isMortgaged()){
                s<< "-1" <<endl;
            } else if (tiles[i]->getLevel() == -100) {
                s <<"0" << endl;
            } else {
                s<<tiles[i]->getLevel()<<endl;
            }
        }
    }
    s.close();
}

void Controller::tgDraw() {
	if(disp_mode < 3) cout << *tg;
    tg.get()->drawWindow();
}

bool Controller::canTradeBuilding(Tile &building) const {
    return building.canTrade();
}
