
#include <sstream>
#include <tuple>
#include <iostream>
#include <algorithm>
#include <tile.h>
#include "text_grid.h"

using namespace std;

TextGrid::TextGrid() {
    tiles_book.clear();
    tiles_book.resize(40, make_tuple("No Name", Normal, 0, BANK, vector<PieceType>{})); // 40 tiles total

    outbuffer.clear();
    outbuffer.resize((unsigned long) (BOARD_SZ_Y));
    for (int i = 0; i < BOARD_SZ_Y; ++i) {
        outbuffer[i].resize((unsigned long) BOARD_SZ_X, ' ');
    }
	// Central wordmark, at y=36, x=24 begin.
	// Ok not leaking
    vector<string> wordmark =
            {
                    R"( _____  _____  ____      ____     )",
                    R"(|_   _||_   _||_  _|    |_  _|  )",
                    R"(  | |    | |    \ \  /\  / /  )",
                    R"(  | '    ' |     \ \/  \/ /   )",
                    R"(   \ \__/ /       \  /\  /    )",
                    R"(    `.__.'         \/  \/      )",
                    R"( ____    ____   ___   ____  _____   ___   )",
                    R"(|_   \  /   _|.'   `.|_   \|_   _|.'   `.  )",
                    R"(  |   \/   | /  .-.  \ |   \ | | /  .-.  \ )",
                    R"(  | |\  /| | | |   | | | |\ \| | | |   | | )",
                    R"( _| |_\/_| |_\  `-'  /_| |_\   |_\  `-'  /  )",
                    R"(|_____||_____|`.___.'|_____|\____|`.___.' )",
                    R"( _______     ___   _____   ____  ____     )",
                    R"(|_   __ \  .'   `.|_   _| |_  _||_  _|     )",
                    R"(  | |__) |/  .-.  \ | |     \ \  / /       )",
                    R"(  |  ___/ | |   | | | |   _  \ \/ /        )",
                    R"( _| |_    \  `-'  /_| |__/ | _|  |_   )",
                    R"(|_____|    `.___.'|________||______|       )",
                    R"()",
                    R"()",
                    R"(             Ali Hussain               )",
                    R"(        Bhargavi Dameracharla          )",
                    R"(          Rickey Wang                  )"
            };
    // For each row
    for (size_t j = 0; j < wordmark.size(); ++j) {
        for (size_t i = 0; i < wordmark.at(j).size(); ++i) {
            outbuffer.at(j+25).at(i+24) = wordmark.at(j).at(i); //@todo Leak here apparently??? [j + 25][i + 24]
        }
    }
}

TextGrid::~TextGrid() {

}

void TextGrid::BookUpdate(){
	int t_idx = 0;
	for(auto t : tiles_book){
		updateTile(get<NAME>(t), get<TILETYPE>(t), t_idx, get<LEVEL>(t), get<OWNER>(t), get<VISITORS>(t));
		++t_idx;
	}
};

/**
 * Do NOT call until the game is ready to be played!
 */
void TextGrid::drawWindow(){
	if(construction_mode || !xw_available) return;
	xw.get()->clearWindow();

	// Update the tiles
	BookUpdate();

	for (int j = 0; j < BOARD_SZ_Y; ++j) {
		string row = "";
		for (int i = 0; i < BOARD_SZ_X; ++i) {
			row += outbuffer[j][i];
		}
		xw.get()->drawString(10,j*10+20,row);
	}
}

/**
 * Apply all updates to the outbuffer and draw the board to ostream specified.
 * @param out
 * @param tg The TextGrid object
 * @return The board
 */
std::ostream &operator<<(std::ostream &out, TextGrid &tg) {
	if(tg.isConstruction_mode()) return out;

    // Update the tiles
	tg.BookUpdate();

    // Print row by row
    for (int j = 0; j < BOARD_SZ_Y; ++j) {
        for (int i = 0; i < BOARD_SZ_X; ++i) {
            out << tg.outbuffer[j][i];
        }
        out << endl;
    }

    return out;
}

vector<int> TextGrid::getDatum(int location_id) {
    int x = 0, y = 0;

    if (location_id >= 31 && location_id <= 39) { // Right edge
        x = TILE_W * 10;
        y = TILE_H * (location_id - 30);
    } else if (location_id >= 21 && location_id <= 30) { // Top Edge
        x = TILE_W * (location_id - 20);
        y = 0;
    } else if (location_id >= 11 && location_id <= 20) { // Left Edge
        x = 0;
        y = TILE_H * (20 - location_id);
    } else if (location_id >= 0 && location_id <= 10) { // Bottom Edge
        x = TILE_W * (10 - location_id);
        y = TILE_H * 10;
    }

    return vector<int>{x, y};
}

/**
 * Call as needed to update a tile
 * @param id Unique ID of the tile
 * @param new_level
 */
void TextGrid::updateTileById(int id, int new_level){
    try {
        get<LEVEL>(tiles_book.at((unsigned long) id)) = new_level;
    } catch (out_of_range &ex) {
        throw runtime_error("In TextGrid, the tile ID specified was out of range");
    }
}

/**
 * Call as needed to update a tile
 * @param id Unique ID of the tile
 * @param new_owner_sym
 */
void TextGrid::updateTileById(int id, PieceType new_owner_sym){
    try{
        get<OWNER>(tiles_book.at((unsigned long) id)) = new_owner_sym;
    } catch (out_of_range &ex) {
        throw runtime_error("In TextGrid, the tile ID specified was out of range");
    }
}

/**
 * Move a piece from one tile to the next. Does not check for errors.
 * @param id Unique ID of the tile
 * @param old_location_id
 * @param new_location_id
 */
void TextGrid::updateTileById(PieceType visitor_sym, int old_location_id, int new_location_id){
    old_location_id = old_location_id % 40;
    new_location_id = new_location_id % 40;
    try{
        auto prev = get<VISITORS>(tiles_book.at((unsigned long) old_location_id));
        prev.erase(remove(prev.begin(), prev.end(), visitor_sym), prev.end());
        get<VISITORS>(tiles_book.at((unsigned long) old_location_id)) = prev;

        get<VISITORS>(tiles_book.at((unsigned long) new_location_id)).push_back(visitor_sym);
    } catch (out_of_range &ex) {
        throw runtime_error("In TextGrid, the tile ID specified was out of range");
    }
}

/**
 * Controller init tiles would call this to initialise all the tiles.
 * Only call once!
 * @param name Tile name
 * @param location_id Unique ID of the tile
 * @param current_level 0 to 5: Level of the tile. -1: Mortaged. -100: Non-ownable
 * @param owner_sym Symbol of owner
 * @param visitor_sym Symbol of visitors
 */
void  TextGrid::constructTile(string name, int location_id, int current_level, PieceType owner_sym,
vector<PieceType> visitor_sym) {
    // Save to book
    TileType  tileType = Normal;
    if(current_level == -100) { tileType = NoInfobox;}
    try{
		tuple<std::string, TileType, int, PieceType, std::vector<PieceType>> t(name, tileType, current_level, owner_sym, visitor_sym);
        tiles_book.at((unsigned long) location_id) = t;
        updateTile(name, tileType, location_id, current_level, owner_sym, visitor_sym);
    } catch (out_of_range &ex) {
        throw runtime_error("In TextGrid, the tile ID specified was out of range");
    }

}

/**
 * Update the tile in outbuffer. Private.
 * @param name
 * @param location_id
 * @param current_level
 * @param owner_sym
 * @param visitor_sym
 */
void TextGrid::updateTile(string name, TileType tileType, int location_id, int current_level, PieceType owner_sym,
                          vector<PieceType> visitor_sym) {
    // Set datum
    vector<int> datum = getDatum(location_id);
    auto tx = [&](const int i) { return i + datum.at(0); };
    auto ty = [&](const int i) { return i + datum.at(1); };

    // Draw the box. See "Box-drawing character" and Block Elements on Wikipedia
    // Vertical separators left and right
	for (int i = 0; i <= TILE_H; ++i) { // <= for bottom edge
		outbuffer[ty(i)][tx(0)] = '|'; // Left │
		outbuffer[ty(i)][tx(9)] = '|'; // Right │
	}
    // Horizontal rules
	for (int i = 1; i < TILE_W; ++i) {
		outbuffer[ty(0)][tx(i)] = '*'; // Top, ▒ Medium shade
		outbuffer[ty(3)][tx(i)] = '-'; // Building Name Box Bottom ┄
		if (tileType != NoInfobox) outbuffer[ty(5)][tx(i)] = '-'; // Info Box Bottom ┄ (not applicable for noInfobox tiles)
		outbuffer[ty(7)][tx(i)] = '*'; // Bottom edge ▒ Medium shade (next tile will over-write this row)
	}

    // Name of building
    vector<string> name_wrapped = wordwrap(name);
    for (size_t i = 0; i < name_wrapped.size(); ++i) {
        for (size_t j = 0; j < name_wrapped.at(i).size(); ++j) {
            outbuffer[ty(i + 1)][tx(j + 1)] = name_wrapped.at(i).at(j);
        }
    }

    if (tileType != NoInfobox) {
        // Status of building
        for (int i = 0; i < 5 ; ++i) { // 5 is max number of improvements
            if(i < current_level) {
				outbuffer[ty(4)][tx(i + 1)] = 'I';
			} else {
				outbuffer[ty(4)][tx(i + 1)] = ' ';
			}
        }
		if (current_level == -1) outbuffer[ty(4)][tx(1)] = 'M'; // Mortgaged
	} else {
		if (current_level == -1){
			outbuffer[ty(4)][tx(1)] = 'M';
		} else {
			outbuffer[ty(4)][tx(1)] = ' ';
		}// Mortgaged
	}

    // Ownership
    outbuffer[ty(4)][tx(8)] = Piece::PieceTypeToChar(owner_sym);

    // Placement (who is on this tile). Always sorted.
    sort(visitor_sym.begin(), visitor_sym.end());
    for (size_t i = 0; i < TILE_W - 1; ++i) {
        if(i < visitor_sym.size()){
            outbuffer[ty(6)][tx((const int) (i + 1))] = Piece::PieceTypeToChar(visitor_sym.at(i));
        } else {
            outbuffer[ty(6)][tx((const int) (i + 1))] = ' ';
        }
    }
}

/**
 * Wraps the input string into vectors. Each vector is a line.
 * Stolen from a2q3
 * @param input Some string
 * @return A vector of lines of that string
 */
std::vector<std::string> TextGrid::wordwrap(std::string input) {
    vector<string> token;
    vector<string> output;

    // Wrap to char required
    // For each word, check if it will fit into this line
    istringstream iss(input);
    string s;
    while (iss >> s) token.push_back(s); // Tokenize
    string line;
    line.clear();
    for (auto word : token) {
        if (line.length() + word.length() < WORD_WRAP_WIDTH) { // Cannot use "<=" here, because we need a space
            if (!line.empty()) line.append(" ");
            line.append(word);
            continue;
        } else {
            // The word is too long. Print the line and empty it.
            if (!line.empty()) output.push_back(line);
            line.clear();
            // Does this word, by itself, fit into the line?
            // Need to split it up
            while (word.length() >= WORD_WRAP_WIDTH) {
                // Print as much as possible on one line
                line = word.substr(0, WORD_WRAP_WIDTH);
                output.push_back(line);
                line.clear();
                // Trim the word until it fits
                word = word.substr(WORD_WRAP_WIDTH, word.length() - 1);
            }

            line.append(word);
        }
    }
    // Don't forget to print the last line
    output.push_back(line);
    return output;
}

void TextGrid::enterConstructionMode() {
	construction_mode = true;
}

void TextGrid::exitConstructionMode() {
	construction_mode = false;
}

bool TextGrid::isConstruction_mode() const {
	return construction_mode;
}

void TextGrid::openXW() {
	xw_available = true;
	xw = make_shared<Xwindow>(XW_WIDTH, XW_HEIGHT);
}

void TextGrid::removePlayerFromTile(PieceType visitor_sym, int location_id) {
	location_id = location_id % 40;
	try{
		auto prev = get<VISITORS>(tiles_book.at((unsigned long) location_id));
		prev.erase(remove(prev.begin(), prev.end(), visitor_sym), prev.end());
		get<VISITORS>(tiles_book.at((unsigned long) location_id)) = prev;
	} catch (out_of_range &ex) {
		throw runtime_error("In TextGrid, the tile ID specified was out of range");
	}
}
