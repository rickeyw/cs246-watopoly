#include "player.h"
#include "tools.h"
#include "controller.h"
#include <vector>
#include <iostream>
using namespace std;

Player::Player(Controller * ctrl): ctrl{ctrl} {
	random_engine.seed(ctrl->getRandomSeed());
}

void Player::testing(Piece &p, vector <string> commands) {
	if (commands[0] == "roll" && commands.size() == 3) {
		string field1 = commands[1];
		string field2 = commands[2];

		if (isint(field1) && isint(field2)) {
			roll(p, stoi(field1), stoi(field2));
		}
	} else if (commands[0] == "move" && commands.size() == 2) {
		string field1 = commands[1];
		Tile * tile = ctrl->getBuildingByName(field1);

		if (tile != nullptr) {
			ctrl->movePiece(p, *tile);
		}
	} else if (commands[0] == "tg" && commands.size() == 1) {
		ctrl->test_tg_cout();
	}  else if (commands[0] == "tgw" && commands.size() == 1) {
		ctrl->tgDraw();
	}else if (commands[0] == "setcash" && commands.size() == 2) {
		string field1 = commands[1];
		
		if (isint(field1)) {
			ctrl->setCash(p, stoi(field1));
		}
	} else {
		ctrl->displayMsg("Invalid Command");
	}
}

void Player::startTurn(Piece &p) {
	ctrl->tgDraw();
	ctrl->displayMsg(p.getName() + "'s turn. (" + Piece::PieceTypeToChar(p.getType()) + ")");
    ctrl->displayMsg("Type help to get a list of commands");
	if(p.getJailTime() > 0){
		ctrl->movePiece(p, 0);
	}

	while (1) {
		string input = ctrl->getInput();
		vector<string> commands = stov(input);
		if (commands.empty()) continue;

		if (commands[0] == "roll" && commands.size() == 1) {
			ctrl->tgDraw();
			roll(p);
		} else if (commands[0] == "next" && commands.size() == 1) {
			ctrl->tgDraw();
			if (next(p)) break;

		} else if (commands[0] == "trade" && commands.size() == 4) {
			string field1 = commands[1];
			string field2 = commands[2];
			string field3 = commands[3];

			ctrl->tgDraw();
			trade(p, field1, field2, field3);

		} else if (commands[0] == "improve" && commands.size() == 3) {
			string field1 = commands[1];
			string field2 = commands[2];

			ctrl->tgDraw();
			improve(p, field1, field2);
		} else if (commands[0] == "mortgage" && commands.size() == 2) {
			string field = commands[1];

			ctrl->tgDraw();
			mortgage(p, field);
		} else if (commands[0] == "unmortgage" && commands.size() == 2) {
			string field = commands[1];

			ctrl->tgDraw();
			unmortgage(p, field);
		} else if (commands[0] == "bankrupt") {
			if (bankrupt(p)) break;
		} else if (commands[0] == "assets") {
			assets(p);

		} else if (commands[0] == "all") {
			all(p);

		} else if (commands[0] == "save" && commands.size() == 2) {
			string field = commands[1];

			ctrl->save(field);
		} else if (commands[0] == "help")  {
			help();
		} else if (ctrl->isTest()) {
			testing(p, commands);
		} else {
			ctrl->displayMsg("Invalid command");
		}
	}
	ctrl->nextTurn();
}

void Player::help() {
	string message;
	message = "roll - Roll dice\n";
	message += "next - End your turn\n";
	message += "trade <player> <1> <2> - Trade <player> <1> for <2>\n";
	message += "improve <building> buy/sell - Buy or sell an improvement on <building>\n";
	message += "unmortgage/mortgage <building> - (un)mortgage <building>\n";
	message += "bankrupt - Declare bankruptcy\n";
	message += "assets - List your assets\n";
	message += "all - List everybodies assets\n";
	message += "save <filename> - Save the game to <filename>";

	ctrl->displayMsg(message);
}

void Player::roll(Piece &p, int d1, int d2) {
	if (ctrl->canPieceRoll(p)) {
		if (d1 == -1 && d2 == -1) {
			d1 = ctrl->dice();
			d2 = ctrl->dice();

		} else if (d1 < 0 || d2 < 0 ) {
			ctrl->displayMsg("Please enter non-negative values");
			return;
		}

		if (d1 == d2) {
			ctrl->displayMsg("You rolled a double!");
			p.addDouble();
		} else {
			p.resetDouble();
		}

		int die = d1 + d2;

		if (p.getDouble() == 3) {
			ctrl->displayMsg("You've rolled three doubles in a row, Go To DC Tims Line.");
			
			Tile *t = ctrl->getBuildingByName("DC TIMS LINE");
			ctrl->setJail(p, 1);
			ctrl->movePiece(p, *t);

			p.resetDouble();
		} else {
			ctrl->walkPiece(p, die);
		}

		if (p.getDouble() == 2) {
			ctrl->displayMsg("You've rolled two doubles in a row, you get an extra roll!");
			return;
		} 

		ctrl->setPieceRoll(p, false);

	} else {
		ctrl->displayMsg("Sorry, you can't roll. Use 'next' to end your turn.");
		return;
	}
	return;
}

bool Player::next(Piece &p) {
	if (ctrl->isBroke(p)) {
		ctrl->displayMsg("You're broke, either get enough money to pass or declare bankruptcy.");
		return false;
	}

	if (!ctrl->canPieceRoll(p)) {
		ctrl->displayMsg("You end your turn");
		if(p.getJailTime() == 0) ctrl->setPieceRoll(p, true);
		
		return true;
	} else {
		ctrl->displayMsg("Sorry, you can't go. Use 'roll' first.");
		return false;
	}
}

void Player::trade(Piece &p, const std::string &name, const std::string &b1, const std::string &b2) {
	Piece * p2 = ctrl->getPieceByName(name);

	if (p2 == nullptr) {
		ctrl->displayMsg(name + " doesn't exist");
		return;
	}

	if (isint(b1) && isint(b2)) {
		ctrl->displayMsg("Sorry, you can't trade money for money.");
		return;
	} else if (isint(b1)) {
		Tile * building = ctrl->getBuildingByName(b2);

		if (building == nullptr) {
			ctrl->displayMsg(b2 + " doesn't exist");
			return;
		}

		int cash = stoi(b1);

		if (!ctrl->owns(*p2, building)) {
			ctrl->displayMsg(name + " does not own " + b2);
			return;
		}

		if (ctrl->getCash(p) < cash) {
			ctrl->displayMsg("You don't have enough money!");
			return;
		}

		if (!ctrl->canTradeBuilding(*building)) {
			ctrl->displayMsg("No building in " + b2 + "'s Monopoly can have improvements.");
			return;
		}

		string question = name + ", do you accept this trade? (y/n)";

		int response = ctrl->askQuestion(question, 0, 1, true);

		if (!response) {
			ctrl->displayMsg(name + " declines the trade!");
			return;
		}

		ctrl->setBuildingOwner(p, *building);

		ctrl->subCash(p, cash);
		ctrl->addCash(*p2, cash);

	} else if (isint(b2)) {
		Tile * building = ctrl->getBuildingByName(b1);

		if (building == nullptr) {
			ctrl->displayMsg(b1 + " doesn't exist");
			return;
		}

		int cash = stoi(b2);

		if (!ctrl->owns(p, building)) {
			ctrl->displayMsg("You do not own " + b1);
			return;
		}

		if (ctrl->getCash(*p2) < cash) {
			ctrl->displayMsg(name + " does not have enough money");
			return;
		}

		if (!ctrl->canTradeBuilding(*building)) {
			ctrl->displayMsg("No building in " + b1 + "'s Monopoly can have improvements.");
			return;
		}

		string question = name + ", do you accept this trade? (y/n)";

		int answer = ctrl->askQuestion(question, 0, 1, true);
		if (!answer) {
			ctrl->displayMsg(name + " declines the trade!");
			return;
		}

		ctrl->setBuildingOwner(*p2, *building);

		ctrl->subCash(*p2, cash);
		ctrl->addCash(p, cash);

	} else {
		Tile * building1 = ctrl->getBuildingByName(b1);
		Tile * building2 = ctrl->getBuildingByName(b2);

		if (building1 == nullptr) {
			ctrl->displayMsg(b1 + " doesn't exist");
			return;
		}

		if (building2 == nullptr) {
			ctrl->displayMsg(b2 + " doesn't exist");
			return;
		}

		if (!ctrl->owns(p, building1)) {
			ctrl->displayMsg("You do not own " + b1);
			return;
		}

		if (!ctrl->owns(*p2, building2)) {
			ctrl->displayMsg(name + " does not own " + b2);
			return;
		}

		if (!ctrl->canTradeBuilding(*building1)) {
			ctrl->displayMsg("No building in " + b1 + "'s Monopoly can have improvements.");
			return;
		}


		if (!ctrl->canTradeBuilding(*building1)) {
			ctrl->displayMsg("No building in " + b2 + "'s Monopoly can have improvements.");
			return;
		}

		string question = name + ", do you accept this trade? (y/n)";

		int answer = ctrl->askQuestion(question, 0, 1, true);

		if (!answer) {
			ctrl->displayMsg(name + " declines the trade!");
			return;
		}

		ctrl->setBuildingOwner(p, *building2);
		ctrl->setBuildingOwner(*p2, *building1);
	}
	ctrl->displayMsg(b1 + " has been traded for " + b2);
	return;
}

void Player::improve(Piece &p, const string &name, const string &buysell) {
	Tile * building = ctrl->getBuildingByName(name);

	if (building == nullptr) {
		ctrl->displayMsg(name + " does not exist");
		return;
	}

	if (!ctrl->owns(p, building)) {
		ctrl->displayMsg("You do not own " + name);
		return;
	}

	if (buysell == "buy") {
		if (!ctrl->canImproveBuilding(*building)) {
			ctrl->displayMsg("You can't improve " + name);
			return;
		}

		int cash = ctrl->getImproveCost(*building);

		if (ctrl->getCash(p) < cash) {
			ctrl->displayMsg("You don't have enough money!");
			return;
		}

		ctrl->improveBuilding(*building);
		ctrl->subCash(p, cash);

		ctrl->displayMsg("You've improved " + name);

	} else if (buysell == "sell") {
		if (ctrl->getImproveLevel(*building) <= 0) {
			ctrl->displayMsg(name + " doesn't have any improvements");
			return;
		}

		ctrl->unimproveBuilding(*building);

		int cash = ctrl->getImproveCost(*building);
		ctrl->addCash(p, cash);

		ctrl->displayMsg("You've sold an improvement on " + name);

	} else {
		ctrl->displayMsg("Please indicate if you'd like to buy or sell");
		return;
	}

	return;
}

void Player::mortgage(Piece &p, const string &name) {
	Tile * building = ctrl->getBuildingByName(name);

	if (building == nullptr) {
		ctrl->displayMsg(name + " does not exist");
		return;
	}

	if (!ctrl->owns(p, building)) {
		ctrl->displayMsg("You do not own " + name);
		return;
	}

	if (ctrl->isMortgaged(*building)) {
		ctrl->displayMsg(name + " is already mortgaged");
		return;
	}

	if(ctrl->getImproveLevel(*building)> 0){
		ctrl->displayMsg("Cannot mortgage a building with improvements. Sell all improvements first. ");
		return;
	}

	ctrl->mortgage(*building);
	
	int cash = ctrl->getCost(*building) / 2;

	ctrl->addCash(p, cash);
	ctrl->displayMsg("You've mortgaged " + name);
	return;
}

void Player::unmortgage(Piece &p, const string &name) {
	Tile * building = ctrl->getBuildingByName(name);

	if (building == nullptr) {
		ctrl->displayMsg(name + " does not exist");
		return;
	}

	if (!ctrl->owns(p, building)) {
		ctrl->displayMsg("You do not own " + name);
		return;
	}

	if (!ctrl->isMortgaged(*building)) {
		ctrl->displayMsg(name + " is not mortgaged");
		return;
	}

	int cash = ctrl->getCost(*building) * 60 / 100;

	if (ctrl->getCash(p) < cash) {
		ctrl->displayMsg("You don't have enough money!");
		return;
	}

	ctrl->unmortgage(*building);
	ctrl->subCash(p, cash);

	ctrl->displayMsg("You've unmortgaged " + name);
	return;
}

bool Player::bankrupt(Piece &p) {
	if (!ctrl->isBroke(p)) {
		ctrl->displayMsg("You're not broke");
		return false;
	}
	
	ctrl->displayMsg(ctrl->getName(p) + " declares bankruptcy!");

	vector <Tile *> buildings = ctrl->getBuildings(p);
	Piece * owing = ctrl->getOwing(p);

	if (owing != nullptr) {
		ctrl->displayMsg(ctrl->getName(p) + " gives all their assets to " + ctrl->getName(*owing));

		for (auto building : buildings) {
			ctrl->setBuildingOwner(*owing, *building);
		}

		int tims = ctrl->numTimsCup(p);

		for (int i {0}; i < tims; ++i) {
			ctrl->giveTimsCup(*owing);
		}

	} else {
		ctrl->displayMsg(ctrl->getName(p) + " puts all their buildings up for auction");

		for (auto building : buildings) {
			auction(*building);
		}

		int tims = ctrl->numTimsCup(p);

		ctrl->subTimsCup(tims);
	}
	ctrl->removeFromGame(p);
	return true;
}

void Player::auction(Tile &building) {
    vector <Piece *> bidders = ctrl->getPieces();
    int currentbid = 0;
    Piece * current_bidder = nullptr;

    if (bidders[0] == ctrl->getTileOwner(building)) {
    	current_bidder = bidders[1];
    } else {
    	current_bidder = bidders[0];
    }

    ctrl->displayMsg(ctrl->getName(building) + " is being auctioned");

    while (1) {
    	if (bidders.size() <= 1) break;

	    for (size_t i {0}; i < bidders.size(); ++i) {
	    	// Not using iterators because erasing gave me so much trouble
	    	Piece * it = bidders[i];

	        string name = ctrl->getName(*it);

	        if (ctrl->getTileOwner(building) == it) {
	        	removePieceFromVector(bidders, it);
	        	continue;
	        }

	        ctrl->displayMsg("It is " + name + "'s bid");
	        
	        if (it == current_bidder) {
	        	ctrl->displayMsg("You have the highest bid already, skipping your turn.");
	        	continue;
	        }

	        if (currentbid > ctrl->getCash(*it)) {
	        	ctrl->displayMsg(name + " does't have enough money!");
	            ctrl->displayMsg(name + " has withdrawn!");
	            removePieceFromVector(bidders, it);
	            continue;
	        }

			string message1 = "The current bid is $" + to_string(currentbid) + " by " + ctrl->getName(*current_bidder) + ". Raise? (y/n)";
	        
	        int response = ctrl->askQuestion(message1, 0, 1, true);

	        if (!response) {
	            ctrl->displayMsg(name + " has withdrawn!");
	            removePieceFromVector(bidders, it);
	            continue;
	        }

	        ctrl->displayMsg(name + ", whats your bid?");
	        int bid;

	        while (1) {
	            string answer = ctrl->getInput();
	            if (!isint(answer)) {
	                ctrl->displayMsg("Please enter a number");
	                continue;
	            }

	            bid = stoi(answer);

	            if (bid <= currentbid) {
	                ctrl->displayMsg("Please RAISE the bid");
	                continue;
	            }

				if (ctrl->getCash(*it) < bid) {
					ctrl->displayMsg("You don't have enough money!");
					continue;
				}

	            break;
	        }

	        currentbid = bid;
	        current_bidder = it;
	        ctrl->displayMsg(name + " has bid $" + to_string(bid));
	    }
	}
    ctrl->displayMsg(ctrl->getName(*current_bidder) + " wins the bid!");
    
    ctrl->subCash(*current_bidder, currentbid);
    ctrl->setBuildingOwner(*current_bidder, building);
}

int Player::die() {
	discrete_distribution<int> fairDieDistribution {1,1,1,1,1,1};
	int die = fairDieDistribution(random_engine) + 1;

    ctrl->displayMsg("You rolled a " + to_string(die));

    return die;
}

void Player::assets(Piece &p) {
	vector <Tile *> buildings = ctrl->getBuildings(p);
	int total_net_worth = 0;

	ctrl->displayMsg(ctrl->getName(p) + "'s assets");

	ctrl->displayMsg("Cash:\t$" + to_string(ctrl->getCash(p)));

	total_net_worth += ctrl->getCash(p);
	ctrl->displayMsg("Buildings:");

	for (auto building : buildings) {
		string bldg_details = ctrl->getName(*building) + "\t";

		if (ctrl->getImproveLevel(*building) >= 0) {
			bldg_details += "L" + to_string(ctrl->getImproveLevel(*building));
		}
		
		bldg_details += "\t $" + to_string(building->getNetValue());

		if (ctrl->isMortgaged(*building)) {
			bldg_details += " (Mortgaged)";
		}

		ctrl->displayMsg(bldg_details);
		total_net_worth += building->getNetValue();
	}

	ctrl->displayMsg("Total Net Worth: $" + to_string(total_net_worth));
	ctrl->displayMsg("Roll up the Rim cups: " + to_string(ctrl->numTimsCup(p)));
}

void Player::all(Piece &p) {
	vector <Piece *> pieces = ctrl->getPieces();

	for (auto &piece : pieces) {
		assets(*piece);
	}
}
