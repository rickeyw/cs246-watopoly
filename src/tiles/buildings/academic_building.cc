#include "academic_building.h"
#include "building_group.h"
using namespace std;

AcademicBuilding::AcademicBuilding(Controller * ctrl, const string &name, BuildingGroup * group, 
		const int cost, vector <int> levels, const int improve_cost):
		Building{ctrl, name, group, cost, levels}, improve_cost{improve_cost} {
			level = 0;
		}

AcademicBuilding::~AcademicBuilding() {}

bool AcademicBuilding::isMonopoly() const {
	int size = group->getSize();
	int owned = group->getBuildingsByOwner(*owner);

	return size == owned;
}

int AcademicBuilding::getLevel() const {
	return level;
}

void AcademicBuilding::addLevel() {
	++level;
}

void AcademicBuilding::subLevel() {
	--level;
}

int AcademicBuilding::getFee() {
	return levels[level];
}

bool AcademicBuilding::canImprove() {
	if (levels.size() - 1 == (size_t)level) {
		return false;
	}

	return isMonopoly();
}

int AcademicBuilding::getImproveCost() const {
	return improve_cost;
}

int AcademicBuilding::getNetValue() const {
	return cost + improve_cost*getLevel();
}

bool AcademicBuilding::canTrade() const {
	if (!isMonopoly()) return true;

	return group->checkNoImproves();
}
