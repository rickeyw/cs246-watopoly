#include "building.h"
#include "building_group.h"
#include "controller.h"
using namespace std;

Building::Building(Controller * ctrl, const string &name, BuildingGroup * group,
	const int cost, std::vector <int> levels):
	Tile{ctrl, name}, group{group}, cost{cost}, levels{levels} {
	
	sold = false;
	mortgaged = false;
	owner = nullptr;
}

Building::~Building() {}

void Building::mortgage() {
	mortgaged = true;
}

void Building::unmortgage() {
	mortgaged = false;
}

bool Building::isMortgaged() const {
	return mortgaged;
}

Piece * Building::getOwner() const {
	return owner;
}

void Building::setOwner(Piece *piece) {
	if (owner != nullptr) {
		owner->removeOwner(this);
	}
	piece->setOwner(this);
	owner = piece;
	setSold(true);
}

bool Building::isSold() const {
	return sold;
}

void Building::setSold(bool value) {
	sold = value;
}

int Building::getCost() const {
	return cost;
}

void Building::visit(Piece &p) {}

void Building::land(Piece &piece) {
	if (!sold) {
		string message = "Would you like to buy this property for $" + to_string(cost) + " (y/n)?";

		int answer = ctrl->askQuestion(message, 0, 1, true);
		
		if (answer == 1) {
			if (ctrl->getCash(piece) < cost) {
				ctrl->displayMsg("You dont have enough money!");
			} else {
				ctrl->subCash(piece, cost);
				ctrl->setBuildingOwner(piece, *this);
				return;
			}
		}
	
		ctrl->auction(*this);

	} else if (mortgaged) {
		ctrl->displayMsg(name + " is mortgaged, you don't pay tuition");
	} else if (owner != &piece) {
		int n = this->getFee();
		
		ctrl->displayMsg(ctrl->getName(*owner) + " owns this building");
		
		int curr_cash = ctrl->getCash(piece);
		ctrl->subCash(piece, n, owner);
		
		int profit = n;

		if (ctrl->isBroke(piece)) {
			profit = curr_cash;
		}

		ctrl->addCash(*owner, profit);
	} else if (owner == &piece) {
		ctrl->displayMsg(ctrl->getName(piece) + " already owns " + name);
	}
}

void Building::setGroup(BuildingGroup *g) {
	group = g;
}

int Building::getNetValue() const {
	return cost;
}
