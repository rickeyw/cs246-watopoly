#include "gym.h"
#include "controller.h"
#include "building_group.h"
using namespace std;

Gym::Gym(Controller * ctrl, const std::string &name, BuildingGroup * group,
		const int cost, std::vector <int> levels):
		Building{ctrl, name, group, cost, levels} {}

Gym::~Gym() {}

int Gym::getFee() {
	int roll = ctrl->dice() + ctrl->dice();
	int owned = group->getBuildingsByOwner(*owner);

	return levels[owned - 1] * roll;
}
