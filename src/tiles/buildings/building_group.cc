#include "building_group.h"
#include "tile.h"
#include "building.h"

using namespace std;

BuildingGroup::BuildingGroup(const std::string &name) : name{name} {}

BuildingGroup::~BuildingGroup() {}

void BuildingGroup::addBuilding(Building * b) {
	buildings.push_back(b);
	b->setGroup(this);
}


int BuildingGroup::getBuildingsByOwner(Piece &piece) {
	int i {0};

	for (auto building : buildings) {
		if (building->getOwner() == &piece) {
			++i;
		}
	}
	return i;
}

string BuildingGroup::getName() const {
	return name;
}

int BuildingGroup::getSize() const {
	return buildings.size();
}

bool BuildingGroup::checkNoImproves() const {
	for (auto building : buildings) {
		if (building->getLevel() != 0) {
			return false;
		}
	}
	return true;
}
