#include "residence.h"
#include "building_group.h"
using namespace std;

Residence::Residence(Controller * ctrl, const std::string &name, BuildingGroup * group, 
		const int cost, std::vector <int> levels):
		Building{ctrl, name, group, cost, levels} {}

Residence::~Residence() {}


int Residence::getFee() {
	int owned = group->getBuildingsByOwner(*owner);

	return levels[owned - 1];
}
