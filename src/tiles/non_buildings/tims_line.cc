#include "tims_line.h"
#include "controller.h"

#include <sstream>

using namespace std;

TimsLine::TimsLine(Controller *ctrl, const std::string &name, const int cost) :
		NonBuilding{ctrl, name}, coffee_price{cost} {}


TimsLine::~TimsLine() {}

void TimsLine::process_repeat_inmate(Piece &pc, int sum_last_roll) {
	string message = "You cannot roll double anymore. Options are: \n";

	message += "1. Pay $50\n";
	message += "2. Use Tims Cup";

	int response = ctrl->askQuestion(message, 1, 2);

	switch (response) {
		case 1:
			ctrl->subCash(pc, coffee_price);
			ctrl->displayMsg("Merci pour votre achat.");
			ctrl->setJail(pc, 0);
			break;
		case 2: {
			if (ctrl->useTimsCup(pc)) {
				ctrl->setJail(pc, 0);
			} else {
				ctrl->displayMsg("You fail. Pay now!");
				ctrl->subCash(pc, coffee_price);
				ctrl->setJail(pc, 0);
				ctrl->displayMsg("You are free... for now");
			}
		}
			break;
		default:
			throw runtime_error("Bad option for Tims line. askQuestion is broken");
	}

	ctrl->walkPiece(pc, sum_last_roll);
	ctrl->setPieceRoll(pc, false);
}

//Lands: nothing unless it's in jail
void TimsLine::land(Piece &pc) {
	//Check if piece was sent to jail
	if (pc.getJailTime() == 0) return;

	//Lock in jail
	ctrl->setPieceRoll(pc, false);

	//Present options and get response
	string message = "Tim Hortons operated by food services\u2122. How would you like to pay?\n";

	message += "1. Roll a Double\n";
	message += "2. Pay $50\n";
	message += "3. Use Tims Cup";

	int response = ctrl->askQuestion(message, 1, 3);

	int rolla = 0, rollb = 0;

	switch (response) {
		case 1:
			rolla = ctrl->dice();
			rollb = ctrl->dice();

			if (rolla == rollb) {
				//freedom!
				ctrl->displayMsg("May the odds be ever in your favour. Good job. ");
				ctrl->setJail(pc, 0);
				ctrl->walkPiece(pc, rolla + rollb);
				ctrl->setPieceRoll(pc, false); // Already moved
			} else {
				if (pc.getJailTime() == 3) { // On third try without rolling double, process.
					process_repeat_inmate(pc, rolla + rollb);
					return;
				}
				ctrl->setJail(pc, pc.getJailTime() + 1);
				ctrl->displayMsg("You fail. Stay here!");
			}
			break;
		case 2:
			ctrl->subCash(pc, coffee_price);
			ctrl->displayMsg("Merci pour votre achat.");
			ctrl->setJail(pc, 0);
			ctrl->setPieceRoll(pc, true);
			break;
		case 3: {
			if (ctrl->useTimsCup(pc)) {
				ctrl->displayMsg("Thank you come again");
				ctrl->setJail(pc, 0);
				ctrl->setPieceRoll(pc, true);
			} else {
				ctrl->displayMsg("You fail. Stay here!");
				ctrl->setJail(pc, pc.getJailTime() + 1);
			}
			}
			break;
		default:
			throw runtime_error("Bad option for Tims line. askQuestion is broken");
	}
}
