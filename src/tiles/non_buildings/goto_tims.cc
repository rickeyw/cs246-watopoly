#include "goto_tims.h"
#include "controller.h"

GotoTims::GotoTims(Controller *ctrl, const std::string &name):
				NonBuilding{ctrl, name} {}

GotoTims::~GotoTims() {}

void GotoTims::land(Piece &p) {
	Tile *t = ctrl->getBuildingByName("DC TIMS LINE");
	ctrl->setJail(p, 1);
	ctrl->movePiece(p, *t);
}

void GotoTims::visit(Piece &p) {
	// Nothing happens
}
