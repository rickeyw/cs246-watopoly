#include "coop_fee.h"
#include "controller.h"

CoopFee::CoopFee(Controller * ctrl, const std::string &name, const int cost):
			NonBuilding{ctrl, name}, cost{cost} {}

CoopFee::~CoopFee() {}

void CoopFee::land(Piece &p) {
	ctrl->subCash(p, cost);
}
