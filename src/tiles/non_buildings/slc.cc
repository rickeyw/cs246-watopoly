#include "slc.h"
#include "controller.h"

using namespace std;

Slc::Slc(Controller *ctrl, const std::string &name, bool debug): NonBuilding{ctrl, name}, debug{debug} {
}

Slc::~Slc() {}

void Slc::land(Piece &p) {

	discrete_distribution<int> discreteDistribution {3,4,4,3,4,4,1,1}; // Weight of 1 here is normalised from 1/24 (multiply probabilities by 24)
	int rand = discreteDistribution(randomEngine);

    double fail = 99;
    double pass = 1;
    if (debug) {
        fail = 50;
        pass = 50;
    }
    discrete_distribution<int> timsCupDist {fail, pass};

	if (timsCupDist(randomEngine)) {
        if (ctrl->giveTimsCup(p)) return;
    }

	switch (rand) {
		case 0:
			ctrl->displayMsg("Move Back 3");
			ctrl->walkPiece(p, -3);
			break;
		case 1:
			ctrl->displayMsg("Move Back 2");
			ctrl->walkPiece(p, -2);
			break;
		case 2:
			ctrl->displayMsg("Move Back 1");
			ctrl->walkPiece(p, -1);
			break;
		case 3:
			ctrl->displayMsg("Move Forward 1");
			ctrl->walkPiece(p, 1);
			break;
		case 4:
			ctrl->displayMsg("Move Forward 2");
			ctrl->walkPiece(p, 2);
			break;
		case 5:
			ctrl->displayMsg("Move Forward 3");
			ctrl->walkPiece(p, 3);
			break;
		case 6:
			ctrl->displayMsg("Go To Tims Line");
			ctrl->setJail(p, 1);
			ctrl->movePiece(p, *ctrl->getBuildingByName("DC TIMS LINE"));
			break;
		case 7:
			ctrl->displayMsg("Advance to Collect OSAP");
			ctrl->walkPiece(p, *ctrl->getBuildingByName("COLLECT OSAP"));
			break;
		default:
			__throw_runtime_error("Random Engine failed in SLC");
	}
}
