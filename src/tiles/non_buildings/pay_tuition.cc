#include <iostream>
#include "pay_tuition.h"
#include "controller.h"

#include <sstream>
using namespace std;

PayTuition::PayTuition(Controller *ctrl, const std::string &name, const int cost):
              NonBuilding{ctrl, name}, cost{cost} {}

PayTuition::~PayTuition() {}

void PayTuition::land(Piece &piece){
    /*
      string question =
        "Pay your tuition! Select an option: \n
        1.Pay $300 cash_money \t 2.Pay 10% of your total worth";
      int resp = ctrl.getResponse(question, 1, 2); // 1 and 2 are min and max values of the response
    */
    ostringstream message;

    message << "Pay your tuition! Select an option:" << endl;
    message << "1. Pay $" << cost << endl;
    message << "2. Pay 10% of your total worth";

    int response = ctrl->askQuestion(message.str(), 1, 2);

    if (response == 1) {
        ctrl->subCash(piece, cost);
    } else if (response == 2) {
        int total_assets  = piece.getCashMoney();
	for (auto tile : piece.getOwned() ) {
		if(tile->getCost() > 0) {
			total_assets += tile->getNetValue();
		}
	}

    ctrl->subCash(piece, (const int) (total_assets * 0.10));
  }
}
