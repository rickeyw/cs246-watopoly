#include "goose_nest.h"
#include "controller.h"

GooseNest::GooseNest(Controller * ctrl, const std::string &name):
				NonBuilding{ctrl, name} {}

GooseNest::~GooseNest() {}

void GooseNest::land(Piece &p) {
	ctrl->displayMsg("You've been attacked by a flock of geese!");
}
