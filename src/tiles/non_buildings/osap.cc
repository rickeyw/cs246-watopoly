#include "osap.h"
#include "controller.h"
#include <string>
using namespace std;

Osap::Osap(Controller * ctrl, const std::string &name, const int money):
		NonBuilding{ctrl, name}, money{money} {}
Osap::~Osap() {}

void Osap::land(Piece &p) {
	ctrl->addCash(p, money);
}

void Osap::visit(Piece &p) {
	ctrl->displayMsg(p.getName() + " visits OSAP, collect $" + to_string(money));
	ctrl->addCash(p, money);
}
