#include "needles_hall.h"
#include "controller.h"

#include <sstream>

using namespace std;

NeedlesHall::NeedlesHall(Controller *ctrl, const std::string &name, bool debug):
                NonBuilding{ctrl, name}, debug{debug} {
}

NeedlesHall::~NeedlesHall() {}

int NeedlesHall::payment_calc() {
    discrete_distribution<int> NHEventsDistribution {1,2,3,6,3,2,1};
    int rand = NHEventsDistribution(randomEngine);
    int delta_cash = 0;

    switch(rand){
        case 0:
            delta_cash = -200;
            break;
        case 1:
            delta_cash = -100;
            break;
        case 2:
            delta_cash = -50;
            break;
        case 3:
            delta_cash = 25;
            break;
        case 4:
            delta_cash = 50;
            break;
        case 5:
            delta_cash = 100;
            break;
        case 6:
            delta_cash = 200;
            break;
        default:
            __throw_runtime_error("Random Engine failed in Needles Hall");
    }

    return delta_cash;
}

void NeedlesHall::land(Piece &piece) {
    double fail = 99;
    double pass = 1;
    if (debug) {
        fail = 50;
        pass = 50;
    }
    discrete_distribution<int> timsCupDist {fail, pass};
	if (timsCupDist(randomEngine)) {
        if (ctrl->giveTimsCup(piece)) return;
    }

    // Discrete distribution, according to Table 3 in assignment
    int payment = payment_calc();
    if(payment < 0) {
        ctrl->displayMsg("You have to pay more Student Fees");
        ctrl->subCash(piece, -payment);
    } else {
        ctrl->displayMsg("You got automatic bursary!");
        ctrl->addCash(piece, payment);
    }
}
