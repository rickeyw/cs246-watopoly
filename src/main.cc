#include <string>
#include <iostream>
#include <unistd.h>
#include <chrono>
#include "controller.h"

using namespace std;

int main(int argc, char **argv){
	std::vector<std::string> all_args(argv, argv + argc);

    bool test_mode = false;
	int disp_mode = 1;
    string save_file = "";
	unsigned long seed = (unsigned long) chrono::system_clock::now().time_since_epoch().count();

    // Retrieve the options:
    for(size_t i = 0; i<all_args.size(); ++i){
        if(all_args.at(i) == "-testing"){
            test_mode = true;
        } else if (all_args.at(i) == "-load") {
            if(i + 1 < argc) {
				save_file.clear();
				save_file = all_args.at(i+1);
            } else {
                fprintf (stderr, "You need to specify a file to use -load\n");
                abort();
            }
        } else if (all_args.at(i) == "-s") {
			if(i + 1 < argc) {
				seed = stoul(all_args.at(i+1));
			} else {
				fprintf (stderr, "You need to specify a seed to use -s\n");
				abort();
			}
		} else if (all_args.at(i) == "-m") {
			if(i + 1 < argc) {
				disp_mode = stoi(all_args.at(i+1));
			} else {
				fprintf (stderr, "You need to specify a Mode to use -m\n");
				abort();
			}
		}
    }

    // Debug:
    if(test_mode){
        cout << "test_mode = " << test_mode << endl;
        cout << "save_file = " << save_file << endl;
    }

    // Initialize the objects
    unique_ptr<Controller> controller(new Controller(test_mode, disp_mode, save_file, seed));

    // Start the game!
    controller.get()->init();

    return 0;
}
