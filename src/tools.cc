#include <sstream>
#include <string>
#include "tools.h"
using namespace std;

char stoc(const string &s) {
    char c;
    istringstream ss {s};
    ss >> c;
    return c;
}

string itos(const int n) {
    ostringstream ss;
    ss << n;
    return ss.str();
}

bool isint(const string &s) {
	int n;
	istringstream ss {s};

	ss >> n;

	return !ss.fail();
}

vector <string> stov(const string &s) {
    istringstream ss {s};
    
    vector <string> strings;
    string n;

    while (ss >> n) {
        strings.push_back(n);
    }

    return strings;
}

void removePieceFromVector(vector <Piece *> & vec, Piece * piece) {
  size_t j = 0;

  for (size_t i = 0; i < vec.size(); ++i) {
    if (vec[i] != piece) {
        vec[j++] = vec[i];
    }
  }
  vec.resize(j);
}
