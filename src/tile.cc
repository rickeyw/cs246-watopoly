#include "tile.h"
#include "controller.h"
#include "piece.h"
using namespace std;

Tile::Tile(Controller *controller, const std::string &name)  : ctrl{controller} , name{name}  {
	randomEngine.seed(ctrl->getRandomSeed());
}
Tile::~Tile() {}
string Tile::getName() const {
	return name;
}

void Tile::visit(Piece &p) {}

vector<Piece *> Tile::getPieces() const {
	return pieces;
}

int Tile::getLevel() const {return -100;} // By default return -100 for tiles that have no getLevel(), like non-buildings

void Tile::addLevel() {return;}
void Tile::subLevel() {return;}
int Tile::getImproveCost() const {return 0;}

void Tile::mortgage() {return;}
void Tile::unmortgage() {return;}
bool Tile::isMortgaged() const {return false;}
int Tile::getCost() const {return 0;}
void Tile::setOwner(Piece*p) {return;}
Piece * Tile::getOwner() const {return nullptr;}
bool Tile::canImprove() {return false;}
int Tile::getNetValue() const {return 0;}
bool Tile::canTrade() const {return true;}
