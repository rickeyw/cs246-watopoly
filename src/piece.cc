#include "piece.h"
using namespace std;

Piece::Piece(PieceType type, const string &name) :
        type{type}, cash_money{1500}, location{0}, jailTime{0}, tims_card{false}, name{name}, can_roll{true} {}

Piece::~Piece() { }

void Piece::setLanded(Tile * landed) {
    this->landed = landed;
}

Tile * Piece::getLanded() {
    return landed;
}

bool Piece::isOwner(Tile * building) const {
    for (auto i : owned) {
        if (i == building) {
            return true;
        }
    }
    return false;
}

void Piece::setOwner(Tile * building) {
    owned.push_back(building);
}

void Piece::removeOwner(Tile * building) {
    for (auto it = owned.begin(); it != owned.end(); ++it) {
        if (*it == building) {
            owned.erase(it);
	    break;
        }
    }
}

int Piece::getCashMoney() const { //Purposely a long name. Not to be confused with setCash in controller.
    return cash_money;
}

void Piece::setCashMoney(const int cash) {
    cash_money = cash;
}

int Piece::getLocation() const {
    return location;
}

void Piece::setLocation(const int location) {
    this->location = location;
}


int Piece::numTimsCard() const {
    return tims_card;
}

void Piece::addTimsCard() {
    ++tims_card;
}

void Piece::subTimsCard() {
    --tims_card;
}

/**
 * Used by graphics to map Piece type to a char.
 */
const char Piece::PieceTypeToChar(PieceType type) {
    switch (type){
        case Goose:
            return 'G';
        case GRT_Bus:
            return 'B';
        case TimsDoughnut:
            return 'D';
        case Prof:
            return 'P';
        case Student:
            return 'S';
        case Money:
            return '$';
        case Laptop:
            return 'L';
        case Pinktie:
            return 'T';
        default:
            return ' ';
    }
}
/**
 * Used by initializer to assign correct PieceType
 * @param type
 * @return
 */
const PieceType Piece::CharToPieceType(char input) { //@todo Make the initializer use this
    switch (input){
        case 'G':
            return Goose;
        case 'B':
            return GRT_Bus;
        case 'D':
            return TimsDoughnut;
        case 'P':
            return Prof;
        case 'S':
            return Student;
        case '$':
            return Money;
        case 'L':
            return Laptop;
        case 'T':
            return Pinktie;
        case ' ':
        default:
            return BANK;
    }
}

bool Piece::canRoll() const {
    return can_roll;
}

void Piece::setRoll(const bool roll) {
    can_roll = roll;
}

int Piece::getJailTime() const {
    return jailTime;
}

void Piece::setJailTime(int jailTime) {
    Piece::jailTime = jailTime;
}

vector <Tile *> Piece::getOwned() const {
    return owned;
}

string Piece::getName() const {
    return name;
}

int Piece::countAssets() const {
    return owned.size();
}

Piece * Piece::getOwing() const {
    return owing;
}

void Piece::setOwing(Piece * piece) {
    owing = piece;
}

PieceType Piece::getType() const {
    return type;
}

void Piece::addDouble() {
    ++double_count;
}

void Piece::resetDouble() {
    double_count = 0;
}

int Piece::getDouble() const {
    return double_count;
}
