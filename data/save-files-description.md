Seed = 33 unless otherwise specified. 

-- Gameplay, SLC, Tuition, Demo
- New game (no save): Argument: seed = 33. 
 - Start game. 2 players. Show cannot have duplicate pieces or be Bank.
 - Watch the game starts.
 - All asset print
 - A: roll 1 1, go to **SLC**, show effects of SLC (move back 1)
 - B: roll 2 2, Pay tuition, $300 option, show cannot see assets. 
 - A rolls 3 3, lands on NH. Lose $100. Get extra roll. Go to jail. Roll double see if can come out. Whatever.
 - B rolls 6 to Tims Line. Show nothing happens. 

-- d1 OSAP, NH, Tuition, Geese, Coop fee demo
- d1s1 One player of each, no tims cup save:
 - Print all assets. 
 - Go to the place and show effects
 - **Tuition**: Show cannot see asset. Choose percent option
 - Print all assets to show changed. 
- d1s2 Two players going to SLC and SLC with always tims cup = true, One person with three cups, save: (seed = 23)
 - Show A failed to get a cup, and move back three steps at SLC
 - Show B have normal effects at NH
 - Highlight that normal behaviour is overriden

-- d2 Tims Demo
- d2s1 Reject save game with location in Go To Tims
- d2s2 Tims Save, A on Jail, B at Jail, t = 1, C near Go To Tims:
 - Differentiate between person ON jail and IN jail. Show A can move, B cannot move. B rolls a double, fails.
 - Show C gets sent to Tims, try to use cup, does not have cup. C improves/trade/mortage something. 
 - Show B gets out by trying to roll doubles, fails, then tries to use cup, fails they pays and leaves
 - Show C gets out by Tims Cup

-- Monopoly/Bldg Group/Mortgage Demo
- d3s1 A without Acad Bldg Monopoly, B owns all rez and gym save:
 - A Cannot level up AL
 - A lands in **gym** (CIF), roll, pay
 - B lands somewhere (UWP?), demonstrate not charged for own tile.
 - A roll 7, lands in **res** (REV), pay
- d3s2 A with  Acad Bldg Monopoly, two bldgs level 1, save:
 - A Try mortgage a L1 bldg (EV1), rejected, **sell improv**, mortgage
 -  B lands DC, show charge higher price
 - Notice 'M' in display
 - B lands on Mortgage, not charged
 - A Unmortgage, notice price charged and display updated
 - A mortgages a gym and res (optional) 
 - Try to trade a building EV1 without improvements but rejected cuz another building in the group is L1
 - Sell the other bldg improv, and now trade
- d3s3  Person with Acad Bldg Monopoly, one bldg level 4, save:
 - **Buy improve** 5 on MC, show cannot upgrade more
 - Cannot trade DC or MC

-- Bankruptcy Demo
- d4s1 A with a mortgaged bldg, $10, B with $10 and one bldg, C with  $2000
 - Show assets
 - A lands on MC (C's tile), cannot pay, bankrupts, C **gets all the asset**. 
 - C does not want to pay for mortgage. A's Bldg remain in mortgage. A dies. 
 - B lands on DC, cannot pay. Bankrupts. 
 - C receives asset and pays for mortgage. B dies. 
 - C wins. **Game ends**
- d4s2 A with a mortgaged bldg, $1, B with $5 and one bldg, C with $2000.
 - Show assets
 - A lands on B's tile (AL), cannot pay, bankrupts, B gets all the asset. (ML)
 - B cannot pay for transfer cost of ML. We wait until B's turn to take care of this. 
 - C lands on B's tile by purely luck! (not really, but you know). C pays B and B pays back to the bank before his turn. 

-- Auction Demo
- d5s1 A with a mortgaged bldg, $1, B with $1000, C with  $2000, D with $2000
 - Show assets
 - A lands on Co-op fee. Cannot pay. Building goes to auction. 
 - B raise, C raise, D raise, B raise, C exit, D exit, B wins with $999, Gets the building. 
 - (optionally, make that bldg mortgaged, so B cannot afford to pay for it)
 - C's turn. C goes to another tile, does not buy it. That tile comes to auction. B cannot afford and turn is skipped automatically. 

-- Bonus
- Show can fit 8 players
